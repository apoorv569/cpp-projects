#pragma once

#include <wx/aui/aui.h>
#include "wx/bitmap.h"
#include <wx/button.h>
#include <wx/choicdlg.h>
#include <wx/dirctrl.h>
#include <wx/event.h>
#include <wx/filedlg.h>
//#include <wx/frame.h>
#include <wx/listbox.h>
#include <wx/listctrl.h>
#include <wx/mediactrl.h>
#include <wx/msgdlg.h>
#include <wx/panel.h>
#include <wx/simplebook.h>
#include <wx/sizer.h>
#include <wx/slider.h>
#include <wx/splitter.h>
#include <wx/stattext.h>
#include <wx/timer.h>
#include <wx/tglbtn.h>

#include <taglib/taglib.h>
#include <taglib/fileref.h>

#include <id3/tag.h>

class Widget
{
//    public:
//        Widget();
//        ~Widget();
/*
    public:
        enum
        {
            PageSmall = 0,
            PageLarge,
        };

        enum PlayerControlIDs
        {
            PCID_SwitchUI = wxID_HIGHEST + 1,

            PCID_Play,
            PCID_Pause,
            PCID_PreviousTrack,
            PCID_NextTrack,
            PCID_Stop,
            PCID_Shuffle,
            PCID_Repeat,
            PCID_Playlist,
            PCID_Mute,
            PCID_SeekBar,
            PCID_VolumeSlider,
            PCID_StaticText,
            PCID_AddTrack,
            PCID_AddDirectory,
            PCID_RemoveTrack,
            PCID_ClearPlaylist,

            PCID_MediaCtrl,
        };
*/
//        wxSimplebook* book;

    public:
/*        wxPanel* sPanel;

        wxFont* font;

        wxBoxSizer* vboxSizer;
        wxBoxSizer* hboxSizer0;
        wxBoxSizer* hboxSizer1;
        wxBoxSizer* hboxSizer2;
        wxBoxSizer* hboxSizer3;
        wxBoxSizer* hboxSizer4;
        wxBoxSizer* hboxSizer5;
        wxBoxSizer* hboxSizer6;
        wxBoxSizer* hboxSizer7;
        wxBoxSizer* hboxSizer8;
        wxBoxSizer* hboxSizer9;
*/
        wxBitmap* play;

        wxButton* playButton;
        wxButton* pauseButton;
        wxButton* stopButton;
        wxButton* previousTrackButton;
        wxButton* nextTrackButton;
        wxButton* addTrackButton;
        wxButton* addFolderButton;
        wxButton* removeTrackButton;
        wxButton* clearPlaylistButton;
        wxButton* libraryViewButton;

        wxStaticText* staticText;

        wxSlider* volumeSlider;
        wxSlider* seekBar;

        wxToggleButton* muteButton;
        wxToggleButton* shuffleButton;
        wxToggleButton* repeatButton;
        wxToggleButton* playlistButton;

        wxFileDialog* filePickerDialogBox;

        wxListBox* playlistBox;

        wxTimer* timer;

        wxMediaCtrl* mediaCtrl;
/*
        // Big view widgets
        wxPanel* bTopPanel;
        wxPanel* bLeftPanel;
        wxPanel* bRightPanel;
//        wxPanel* bBottomPanel;
        wxSplitterWindow* SplitterWindowTop;
        wxSplitterWindow* SplitterWindowBottom;
        wxGenericDirCtrl* DirCtrl;
        wxListCtrl* ListCtrl;
        wxBoxSizer* bMainSizer;
        wxBoxSizer* bDirCtrlSizer;
        wxBoxSizer* bListCtrlSizer;
        wxBoxSizer* bPanelMainSizer;
        wxBoxSizer* bPanelBottomSizer;
        wxBoxSizer* bPanelBottomLeftSizer;
        wxBoxSizer* bPanelBottomRightSizer;
        wxBoxSizer* bPlayerButtonSizer;
        wxBoxSizer* bPlayerStaticTextSizer;
        wxBoxSizer* bPlayerSeekSliderSizer;
        wxBoxSizer* bPlayerRightSizer;
        wxBoxSizer* bPlayerVolumeSliderSizer;
        wxBoxSizer* bPlayerRepeatShuffleSizer;

        wxButton* SomeButton;
        wxButton* SettingsButton;
//        wxAuiManager* bAuiManage;
*/
/*    public:
        bool paused;
        bool stopped;
        bool muted;
        bool shuffle;
        bool repeat;

        int mode_count;

    public:
        void Play(wxCommandEvent& event);
        void Pause(wxCommandEvent& event);
        void PreviousTrack(wxCommandEvent& event);
        void NextTrack(wxCommandEvent& event);
        void Stop(wxCommandEvent& event);
        void UpdateElapsedTime(wxTimerEvent& event);
        void SeekBar(wxScrollEvent& event);
        void Volume(wxScrollEvent& event);
        void Mute(wxCommandEvent& event);
        void AddSong(wxCommandEvent& event);
//        void AddFolder(wxCommandEvent& event);
        void RemoveSong(wxCommandEvent& event);
        void ClearPlaylist(wxCommandEvent& event);
        void Shuffle(wxCommandEvent& event);
        void Repeat(wxCommandEvent& event);
        void Playlist(wxCommandEvent& event);
*/};
