#pragma once

#include "wx/event.h"
#include "wx/statbmp.h"

#include "PCID_Enums.hpp"
#include "Widgets.hpp"

class BigPlayer : public wxPanel, public Widget
{
    public:
        BigPlayer(wxWindow* window);
        ~BigPlayer();

    public:
/*        wxButton* playButton;
        wxButton* pauseButton;
        wxButton* stopButton;
        wxButton* previousTrackButton;
        wxButton* nextTrackButton;
        wxToggleButton* muteButton;
        wxStaticText* staticText;
        wxSlider* volumeSlider;
        wxSlider* seekBar;

        wxToggleButton* shuffleButton;
        wxToggleButton* repeatButton;
        wxToggleButton* playlistButton;

        wxButton* addTrackButton;
        wxButton* addFolderButton;
        wxButton* removeTrackButton;
        wxButton* clearPlaylistButton;
        wxButton* libraryViewButton;
        wxFileDialog* filePickerDialogBox;
*/
        // Big view widgets
        wxPanel* bTopPanel;
        wxPanel* bLeftPanel;
        wxPanel* bRightPanel;
//        wxPanel* bBottomPanel;

        wxSplitterWindow* SplitterWindowTop;
        wxSplitterWindow* SplitterWindowBottom;

        wxGenericDirCtrl* DirCtrl;
        wxListCtrl* ListCtrl;
        wxNotebook* AddSortView;

        wxStaticBitmap* ArtworkDisplay;

        wxBoxSizer* bMainSizer;
        wxBoxSizer* bDirCtrlSizer;
        wxBoxSizer* bListCtrlSizer;
        wxBoxSizer* bPanelMainSizer;
        wxBoxSizer* bPanelBottomSizer;
        wxBoxSizer* bPanelBottomLeftSizer;
        wxBoxSizer* bPanelBottomRightSizer;
        wxBoxSizer* bPlayerButtonSizer;
        wxBoxSizer* bPlayerStaticTextSizer;
        wxBoxSizer* bPlayerSeekSliderSizer;
        wxBoxSizer* bPlayerLeftSizer;
        wxBoxSizer* bPlayerMidSizer;
        wxBoxSizer* bPlayerRightSizer;
        wxBoxSizer* bPlayerVolumeSliderSizer;
        wxBoxSizer* bPlayerRepeatShuffleSizer;

        wxButton* SomeButton;
        wxButton* SettingsButton;
//        wxAuiManager* bAuiManage;

    public:
        void AddSong(wxCommandEvent& event);
};
