#include "BigPlayer.hpp"
#include "wx/defs.h"
#include "wx/gdicmn.h"
#include "wx/gtk/bitmap.h"

BigPlayer::BigPlayer(wxWindow* window): wxPanel(window, wxID_ANY, wxDefaultPosition, wxDefaultSize)
{
    // Initializing BoxSizers
    bMainSizer = new wxBoxSizer(wxVERTICAL);
    bPanelMainSizer = new wxBoxSizer(wxHORIZONTAL);
    bPanelBottomSizer = new wxBoxSizer(wxHORIZONTAL);
    bPanelBottomLeftSizer = new wxBoxSizer(wxHORIZONTAL);
    bPanelBottomRightSizer = new wxBoxSizer(wxHORIZONTAL);
    bDirCtrlSizer = new wxBoxSizer(wxVERTICAL);
    bListCtrlSizer= new wxBoxSizer(wxVERTICAL);
    bPlayerButtonSizer = new wxBoxSizer(wxHORIZONTAL);
    bPlayerStaticTextSizer = new wxBoxSizer(wxHORIZONTAL);
    bPlayerSeekSliderSizer = new wxBoxSizer(wxHORIZONTAL);
    bPlayerLeftSizer = new wxBoxSizer(wxVERTICAL);
    bPlayerMidSizer = new wxBoxSizer(wxVERTICAL);
    bPlayerRightSizer = new wxBoxSizer(wxVERTICAL);
    bPlayerVolumeSliderSizer = new wxBoxSizer(wxHORIZONTAL);
    bPlayerRepeatShuffleSizer = new wxBoxSizer(wxHORIZONTAL);

    // Creating top splitter window,
    // this will be used to drawing the player controls panel,
    // and another splitter that will hold a wxNotebook and wxListCtrl.
    SplitterWindowTop = new wxSplitterWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D, _T("SPLIT TOP"));
    SplitterWindowTop->SetMinimumPaneSize(10);
    SplitterWindowTop->SetSashGravity(0.2);

    // Top half of the splitter window,
    // this panel will hold all the player controls,
    // like play, pause, stop buttons,
    // seek bar, volume control etc.
    bTopPanel = new wxPanel(SplitterWindowTop, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("PLAYER"));

    // Creating bottom half of the top splitter window,
    // which is another splitter window,
    // both its splits will hold a panel for showing
    // other widgets.
    SplitterWindowBottom = new wxSplitterWindow(SplitterWindowTop, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D, _T("SPLIT BOTTOM"));
    SplitterWindowBottom->SetMinimumPaneSize(10);
    SplitterWindowBottom->SetSashGravity(0.2);

    // Left half of bottom splitter window,
    // this panel will hold wxNotebook,
    // which will have 2 pages,
    // browse and sort.
    bLeftPanel = new wxPanel(SplitterWindowBottom, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("DIRECTORY"));

    AddSortView = new wxNotebook(bLeftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, _T("NOTEBOOK"));

    // Initializing DirCtrl
    DirCtrl = new wxGenericDirCtrl(AddSortView,
                                   wxID_ANY,
                                   wxDirDialogDefaultFolderStr,
                                   wxDefaultPosition,
                                   wxDefaultSize,
                                   wxDIRCTRL_3D_INTERNAL | wxSUNKEN_BORDER,
                                   wxEmptyString, 0);
    DirCtrl->ShowHidden(false);

    AddSortView->AddPage(DirCtrl, "Browser", false);

    // Right half of the bottom splitter window,
    // this panel will hold wxListCtrl,
    // to show added tracks.
    bRightPanel = new wxPanel(SplitterWindowBottom, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("LIBRARY"));

    // Initializing ListCtrl
    ListCtrl = new wxListCtrl(bRightPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT);

    // Setting which splitter window
    // should split in which direction
    SplitterWindowTop->SplitHorizontally(bTopPanel, SplitterWindowBottom);
    SplitterWindowBottom->SplitVertically(bLeftPanel, bRightPanel);

    ArtworkDisplay = new wxStaticBitmap(bTopPanel, wxID_ANY, wxBitmap(("/home/apoorv/resized.png"), wxBITMAP_TYPE_PNG));

    // Initializing player controls
    playButton = new wxButton(bTopPanel, PCID_Play, wxT("|>"), wxDefaultPosition, wxDefaultSize, 0);
    pauseButton = new wxButton(bTopPanel, PCID_Pause, wxT("||"), wxDefaultPosition, wxDefaultSize, 0);
    stopButton = new wxButton(bTopPanel, PCID_Stop, wxT("[]"), wxDefaultPosition, wxDefaultSize, 0);
    previousTrackButton = new wxButton(bTopPanel, PCID_PreviousTrack, wxT("<<"), wxDefaultPosition, wxDefaultSize, 0);
    nextTrackButton = new wxButton(bTopPanel, PCID_NextTrack, wxT(">>"), wxDefaultPosition, wxDefaultSize, 0);
    muteButton = new wxToggleButton(bTopPanel, PCID_Mute, wxT("<))"), wxDefaultPosition, wxDefaultSize, 0);

    staticText = new wxStaticText(bTopPanel, PCID_StaticText, wxT("--:--/--:--"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
//    staticText->SetFont(*font);

    volumeSlider = new wxSlider(bTopPanel, PCID_VolumeSlider, 100, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
    seekBar = new wxSlider(bTopPanel, PCID_SeekBar, 0, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);

    shuffleButton = new wxToggleButton(bTopPanel, PCID_Shuffle, wxT("Shuffle off"), wxDefaultPosition, wxDefaultSize, 0);
    repeatButton = new wxToggleButton(bTopPanel, PCID_Repeat, wxT("Repeat off"), wxDefaultPosition, wxDefaultSize, 0);

    addTrackButton = new wxButton(this, PCID_AddTrack, wxT("+"), wxDefaultPosition, wxDefaultSize, 0);
    addFolderButton = new wxButton(this, PCID_AddDirectory, wxT("*"), wxDefaultPosition, wxDefaultSize, 0);
    removeTrackButton = new wxButton(this, PCID_RemoveTrack, wxT("-"), wxDefaultPosition, wxDefaultSize, 0);
    clearPlaylistButton = new wxButton(this, PCID_ClearPlaylist, wxT("C"), wxDefaultPosition, wxDefaultSize, 0);

    SomeButton = new wxButton(this, wxID_ANY, "Some Button", wxDefaultPosition, wxDefaultSize, 0);
    SettingsButton = new wxButton(this, wxID_ANY, "Settings", wxDefaultPosition, wxDefaultSize, 0);
    libraryViewButton = new wxButton(this, PCID_SwitchUI, "Compact Mode", wxDefaultPosition, wxDefaultSize, 0);

    // Binding events
/*    Bind(wxEVT_BUTTON, &Widget::Play, this, PCID_Play);
    Bind(wxEVT_BUTTON, &Widget::Pause, this, PCID_Pause);
    Bind(wxEVT_BUTTON, &Widget::Stop, this, PCID_Stop);
    Bind(wxEVT_BUTTON, &Widget::PreviousTrack, this, PCID_PreviousTrack);
    Bind(wxEVT_BUTTON, &Widget::NextTrack, this, PCID_NextTrack);

//    volumeSlider->Connect(wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(Widget::Volume), NULL, this);
//    seekBar->Connect(wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(Widget::SeekBar), NULL, this);

    Bind(wxEVT_TOGGLEBUTTON, &Widget::Mute, this, PCID_Mute);
    Bind(wxEVT_TOGGLEBUTTON, &Widget::Shuffle, this, PCID_Shuffle);
    Bind(wxEVT_TOGGLEBUTTON, &Widget::Repeat, this, PCID_Repeat);

    Bind(wxEVT_BUTTON, &BigPlayer::AddSong, this, PCID_AddTrack);
    Bind(wxEVT_BUTTON, &Widget::RemoveSong, this, PCID_RemoveTrack);
    Bind(wxEVT_BUTTON, &Widget::ClearPlaylist, this, PCID_ClearPlaylist);
*/
    bPlayerLeftSizer->Add(ArtworkDisplay, 1, wxALL | wxALIGN_CENTER_VERTICAL, 2);

    // Adding player controls to their sizers,
    // so they fit and scale according to them.
    bPlayerButtonSizer->Add(previousTrackButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 2);
    bPlayerButtonSizer->Add(playButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 2);
    bPlayerButtonSizer->Add(pauseButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    bPlayerButtonSizer->Add(stopButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    bPlayerButtonSizer->Add(nextTrackButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 2);

    bPlayerStaticTextSizer->Add(staticText, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);
    bPlayerSeekSliderSizer->Add(seekBar, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);

    bPlayerMidSizer->Add(bPlayerStaticTextSizer, 1, wxALL | wxEXPAND, 2);
    bPlayerMidSizer->Add(bPlayerSeekSliderSizer, 1, wxALL | wxEXPAND, 2);
    bPlayerMidSizer->Add(bPlayerButtonSizer, 1, wxALL | wxEXPAND, 2);

    bPlayerVolumeSliderSizer->Add(muteButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 2);
    bPlayerVolumeSliderSizer->Add(volumeSlider, 2, wxALL | wxALIGN_CENTER_VERTICAL, 2);

    bPlayerRepeatShuffleSizer->Add(shuffleButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 2);
    bPlayerRepeatShuffleSizer->Add(repeatButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 2);

    bPlayerRightSizer->Add(bPlayerVolumeSliderSizer, 1, wxALL | wxEXPAND, 2);
    bPlayerRightSizer->Add(bPlayerRepeatShuffleSizer, 1, wxALL | wxEXPAND, 2);

    bPanelMainSizer->Add(bPlayerLeftSizer, 1, wxALL | wxEXPAND, 2);
    bPanelMainSizer->Add(bPlayerMidSizer, 3, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);
    bPanelMainSizer->Add(bPlayerRightSizer, 1, wxALL | wxEXPAND, 2);

    bDirCtrlSizer->Add(AddSortView, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxEXPAND, 2);

    bListCtrlSizer->Add(ListCtrl, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxEXPAND, 2);

    bPanelBottomLeftSizer->Add(addTrackButton, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);
    bPanelBottomLeftSizer->Add(addFolderButton, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);
    bPanelBottomLeftSizer->Add(removeTrackButton, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);
    bPanelBottomLeftSizer->Add(clearPlaylistButton, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);
    bPanelBottomLeftSizer->Add(SomeButton, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);

    bPanelBottomRightSizer->Add(SettingsButton, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);
    bPanelBottomRightSizer->Add(libraryViewButton, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);

    bPanelBottomSizer->Add(bPanelBottomLeftSizer, 3, wxALL | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL, 2);
    bPanelBottomSizer->Add(bPanelBottomRightSizer, 1, wxALL | wxEXPAND, 2);

    bMainSizer->Add(SplitterWindowTop, 1, wxALL | wxEXPAND, 2);
    bMainSizer->Add(bPanelBottomSizer, 0, wxALL | wxEXPAND, 2);

    // Setting which sizer the player controls panel should use,
    // and adjusting some properties of the sizer,
    // so it scales properly.
    bTopPanel->SetSizer(bPanelMainSizer);
    bPanelMainSizer->Fit(bTopPanel);
    bPanelMainSizer->SetSizeHints(bTopPanel);
    bPanelMainSizer->Layout();

    // Setting which sizer the DirCtrl panel should use,
    // and adjusting some properties of the sizer,
    // so it scales properly.
    bLeftPanel->SetSizer(bDirCtrlSizer);
    bDirCtrlSizer->Fit(bLeftPanel);
    bDirCtrlSizer->SetSizeHints(bLeftPanel);
    bDirCtrlSizer->Layout();

    // Setting which sizer the ListCtrl panel should use,
    // and adjusting some properties of the sizer,
    // so it scales properly.
    bRightPanel->SetSizer(bListCtrlSizer);
    bListCtrlSizer->Fit(bRightPanel);
    bListCtrlSizer->SetSizeHints(bRightPanel);
    bListCtrlSizer->Layout();

    // Setting which sizer the top panel should use,
    // and adjust some properties of the sizer,
    // so it scales properly.
    this->SetSizer(bMainSizer);
    bMainSizer->Fit(this);
    bMainSizer->SetSizeHints(this);
    this->Layout();

//////////////////////////// END OF UI DESIGN ///////////////////////////////////

//    const char* Song;
//    wxString wxSong;

//    wxSong = DirCtrl->GetFilePath();

//    Song = wxSong.mb_str();

//    std::cout << "Song: " << Song << std::endl;

//    TagLib::FileRef file("/home/apoorv/Music/mod/Moderator - As The Lights Fade - 01 It's A Jazzy Thing.ogg");
//    TagLib::uint trackno = file.tag()->track();
//    TagLib::String title = file.tag()->title();
//    TagLib::String artist = file.tag()->artist();
//    TagLib::String album = file.tag()->album();
//    TagLib::String genre = file.tag()->genre();

//    std::cout << "Song: " << title << std::endl;

//    ID3_Tag myTag("/home/apoorv/Music/mod/Moderator - As The Lights Fade - 01 It's A Jazzy Thing.ogg");
//    ID3_Frame* frame = myTag.Find(ID3FID_TITLE);
//    ID3_Field* field = NULL;
//    std::cout << "Frame: " << frame << std::endl;

//    char str1[1024];
//    const char* p1 = "My String";
//    const char* p2 = "My Other String";

//    field->Set(p1);
//    (*field) = p2;  // equivalent to Set

//    field->Get(str1, 1024); // copies up to 1024 bytes of the field data into str1
//    p1 = field->GetRawText(); // returns a pointer to the internal string

//    std::cout << "Field: " << field << std::endl;
//    std::cout << "String: " << str1 << std::endl;
//    std::cout << "P1:  " << p1 << std::endl;
//    std::cout << "P2: " << p2 << std::endl;

    ListCtrl->AppendColumn("#");
    ListCtrl->AppendColumn("Title");
    ListCtrl->AppendColumn("Artist");
    ListCtrl->AppendColumn("Album");
    ListCtrl->AppendColumn("Genre");

    ListCtrl->SetColumnWidth(0, 50);
    ListCtrl->SetColumnWidth(1, 200);
    ListCtrl->SetColumnWidth(2, 200);
    ListCtrl->SetColumnWidth(3, 200);
    ListCtrl->SetColumnWidth(4, 180);
}

void BigPlayer::AddSong(wxCommandEvent& event)
{
    filePickerDialogBox = new wxFileDialog(this,
                                   wxFileSelectorPromptStr,
                                   wxEmptyString,
                                   wxEmptyString,
                                   "OGG files (*.ogg)|*.ogg",
                                   wxFD_DEFAULT_STYLE);

    if (filePickerDialogBox->ShowModal() == wxID_CANCEL)
    {
        return;
    }
    else if (filePickerDialogBox->ShowModal() == wxID_OK)
    {
        ListCtrl->InsertItem(1, filePickerDialogBox->GetFilename());
    }
}

BigPlayer::~BigPlayer(){}
