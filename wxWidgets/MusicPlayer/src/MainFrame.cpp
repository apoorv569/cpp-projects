#include "MainFrame.hpp"

MainFrame::MainFrame(): wxFrame(NULL, wxID_ANY, "Music Player", wxDefaultPosition, wxSize(500, 600))
{
    book = new wxSimplebook(this);

    // Start with the small panel displayed
    book->AddPage(new SmallPlayer(book), "SmallPlayer", true);
    book->AddPage(new BigPlayer(book), "BigPlayer");

//    command = new Command(this);

    // Binding button event to functions
    Bind(wxEVT_BUTTON, &MainFrame::SwitchLayout, this, PCID_SwitchUI);

//    Bind(wxEVT_BUTTON, &Widget::Play, this, PCID_Play);
//    Bind(wxEVT_BUTTON, &Widget::Pause, this, PCID_Pause);
//    Bind(wxEVT_BUTTON, &Widget::PreviousTrack, this, PCID_PreviousTrack);
//    Bind(wxEVT_BUTTON, &Widget::NextTrack, this, PCID_NextTrack);
//    Bind(wxEVT_BUTTON, &Widget::Stop, this, PCID_Stop);
//    Bind(wxEVT_BUTTON, &Widget::Mute, this, PCID_Mute);

//    Bind(wxEVT_SCROLL_THUMBTRACK, &Widget::Volume, this, PCID_VolumeSlider);

//    Bind(wxEVT_BUTTON, &Widget::AddSong, this, PCID_AddTrack);

}

void MainFrame::SwitchLayout(wxCommandEvent &event)
{
    if ( book->GetSelection() == PageSmall )
    {
        book->SetSelection(PageLarge);
        SetSize(wxSize(800, 600));
    }
    else
    {
        book->SetSelection(PageSmall);
        SetSize(wxSize(500, 600));
    }
}

MainFrame::~MainFrame(){}
