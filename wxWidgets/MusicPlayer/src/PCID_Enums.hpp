#pragma once

#include <wx/defs.h>

enum PlayerControlIDs
{
    PCID_SwitchUI = wxID_HIGHEST + 1,

    PCID_Play,
    PCID_Pause,
    PCID_PreviousTrack,
    PCID_NextTrack,
    PCID_Stop,
    PCID_Shuffle,
    PCID_Repeat,
    PCID_Playlist,
    PCID_Mute,
    PCID_SeekBar,
    PCID_VolumeSlider,
    PCID_StaticText,
    PCID_AddTrack,
    PCID_AddDirectory,
    PCID_RemoveTrack,
    PCID_ClearPlaylist,

    PCID_MediaCtrl,
};
