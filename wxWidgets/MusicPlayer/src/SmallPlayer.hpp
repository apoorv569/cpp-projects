#pragma once

#include "PCID_Enums.hpp"
#include "Widgets.hpp"

class SmallPlayer : public wxPanel, public Widget
{
    public:
        SmallPlayer(wxWindow* window);
        ~SmallPlayer();

    public:
        bool paused;
        bool stopped;
        bool muted;
        bool shuffle;
        bool repeat;

        int mode_count;

   public:
        wxPanel* sPanel;

        wxFont* font;

        wxBoxSizer* vboxSizer;
        wxBoxSizer* hboxSizer0;
        wxBoxSizer* hboxSizer1;
        wxBoxSizer* hboxSizer2;
        wxBoxSizer* hboxSizer3;
        wxBoxSizer* hboxSizer4;
        wxBoxSizer* hboxSizer5;
        wxBoxSizer* hboxSizer6;
        wxBoxSizer* hboxSizer7;
        wxBoxSizer* hboxSizer8;
        wxBoxSizer* hboxSizer9;

   public:
        void Play(wxCommandEvent& event);
        void Pause(wxCommandEvent& event);
        void PreviousTrack(wxCommandEvent& event);
        void NextTrack(wxCommandEvent& event);
        void Stop(wxCommandEvent& event);
        void UpdateElapsedTime(wxTimerEvent& event);
        void SeekBar(wxScrollEvent& event);
        void Volume(wxScrollEvent& event);
        void Mute(wxCommandEvent& event);
        void AddSong(wxCommandEvent& event);
        void AddFolder(wxCommandEvent& event);
        void RemoveSong(wxCommandEvent& event);
        void ClearPlaylist(wxCommandEvent& event);
        void Shuffle(wxCommandEvent& event);
        void Repeat(wxCommandEvent& event);
        void Playlist(wxCommandEvent& event);
//        void SwitchLayout(wxCommandEvent& event);
};
