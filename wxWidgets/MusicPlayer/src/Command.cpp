#include "Widgets.hpp"

Widget::Widget()
{
    paused = false;
    stopped = false;
    muted = false;

    mode_count = 0;
}
/*
void Widget::AddSong(wxCommandEvent& event)
{
    if (filePickerDialogBox->ShowModal() == wxID_CANCEL)
    {
        return;
    }
    else if (filePickerDialogBox->ShowModal() == wxID_OK)
    {
        playlistBox->Append(filePickerDialogBox->GetFilename());
    }
}
*/
void Widget::Play(wxCommandEvent& event)
{
    stopped = false;

    int songIndex;

    std::string Song;
    wxString wxSong;
/*
    if (Widget::mediaCtrl->Tell() == Widget::mediaCtrl->Length())
    {
        mediaCtrl->Seek(wxFromStart);
        Play(event);
        if ((mode_count == 0 && shuffle) || (mode_count == 2 && shuffle))
        {
            std::cout << "-- Repeat off/all and shuffle --" << std::endl;
            std::srand(time(0));
            songIndex = std::rand() % Widget::playlistBox->GetCount();
            wxSong = Widget::playlistBox->GetString(songIndex);
            playlistBox->SetSelection(songIndex);
        }
        else if ((mode_count == 0 && !shuffle) || (mode_count == 2 && !shuffle))
        {
            std::cout << "-- Repeat off/all without shuffle --" << std::endl;
            songIndex = Widget::playlistBox->GetSelection();
            wxSong = Widget::playlistBox->GetString(songIndex);
        }
        else if ((mode_count == 1 && shuffle) || (mode_count == 1 && !shuffle))
        {
            std::cout << "-- Repeat one --" << std::endl;
            songIndex = Widget::playlistBox->GetSelection();
            wxSong = Widget::playlistBox->GetString(songIndex);
        }
        else
        {
            std::cout << "Error." << std::endl;
        }
    }
*/
    songIndex = playlistBox->GetSelection();
    wxSong = playlistBox->GetString(songIndex);
    Song = std::string(wxSong);
    Song = "/home/apoorv/Music/mod/" + Song;

    std::cout << "wxString: " << wxSong << std::endl;
    std::cout << "Song: " << Song << std::endl;
    std::cout << mode_count << std::endl;

    mediaCtrl->Load(Song);
    mediaCtrl->Play();

//    std::cout << "Controls: " << Widget::mediaCtrl->ShowPlayerControls(wxMEDIACTRLPLAYERCONTROLS_DEFAULT) << std::endl;

    timer->Start(PCID_Play);
}

void Widget::Pause(wxCommandEvent& event)
{
    if (!stopped)
    {
        if (paused)
        {
            std::cout << mediaCtrl->Tell() << std::endl;
            mediaCtrl->Seek(mediaCtrl->Tell());
            mediaCtrl->Play();
            paused = false;
        }
        else if (!paused)
        {
            mediaCtrl->Pause();
            paused = true;
        }
    }
    else { }
}

void Widget::PreviousTrack(wxCommandEvent& event)
{
    int prevSongIndex = playlistBox->GetSelection()-1;

    std::string Song;
    wxString wxprevSong;

    wxprevSong = playlistBox->GetString(prevSongIndex);
    Song = std::string(wxprevSong);

    Song = "/home/apoorv/Music/mod/" + wxprevSong;

    playlistBox->SetSelection(prevSongIndex);

    mediaCtrl->Load(Song);
    mediaCtrl->Play();
}

void Widget::NextTrack(wxCommandEvent& event)
{
    int nextSongIndex = playlistBox->GetSelection()+1;

    std::string Song;
    wxString wxnextSong;

    wxnextSong = playlistBox->GetString(nextSongIndex);
    Song = std::string(wxnextSong);

    Song = "/home/apoorv/Music/mod/" + wxnextSong;

    playlistBox->SetSelection(nextSongIndex);

    mediaCtrl->Load(Song);
    mediaCtrl->Play();
}

void Widget::Stop(wxCommandEvent& event)
{
    mediaCtrl->Stop();
    stopped = true;

}
/*
void Widget::UpdateElapsedTime(wxTimerEvent& event)
{
    seekBar->SetValue(mediaCtrl->Tell());
    seekBar->SetMax(mediaCtrl->Length());

    std::string SongTime = std::to_string(mediaCtrl->Tell() / 60000) + ":" + std::to_string(mediaCtrl->Tell() / 1000)
        + "/" + std::to_string(mediaCtrl->Length() / 60000) + ":" + std::to_string(mediaCtrl->Length() / 1000);

    staticText->SetLabel(SongTime);

    if (mediaCtrl->Tell() == mediaCtrl->Length())
    {
        std::cout << "Song ended" << std::endl;
        mediaCtrl->Seek(wxFromStart);
        mediaCtrl->Play();
    }

    std::cout << "Tell: " << mediaCtrl->Tell() << std::endl;
    std::cout << "Length: " << mediaCtrl->Length() << std::endl;
}
*/
void Widget::SeekBar(wxScrollEvent& event)
{
    mediaCtrl->Seek(Widget::seekBar->GetValue());

    std::cout << "MEDIACTRL SEEK POS: " <<  mediaCtrl->Tell() / 60000 << ":" << mediaCtrl->Tell() / 1000 << std::endl;
    std::cout << "SLIDER SEEK POS: " << seekBar->GetValue() << std::endl;
}

void Widget::Volume(wxScrollEvent& event)
{
    double getVolume = mediaCtrl->GetVolume() * 100.0;

    std::cout << "wxMediaCtrl Vol: " << getVolume << std::endl;
    std::cout << "Slider Vol: " << volumeSlider->GetValue() << std::endl;

    mediaCtrl->SetVolume(static_cast<double>(volumeSlider->GetValue()) / 100);
}

void Widget::Mute(wxCommandEvent& event)
{
    int getVolume = mediaCtrl->GetVolume() * 100;
    if (!muted)
    {
        mediaCtrl->SetVolume(0);
        muteButton->SetValue(true);
        std::cout << "muted: " << getVolume << std::endl;
        muted = true;
    }
    else if (muted)
    {
        mediaCtrl->SetVolume(1);
        muteButton->SetValue(false);
        std::cout << "unmuted: " << getVolume << std::endl;
        muted = false;
    }
}

void Widget::RemoveSong(wxCommandEvent& event)
{
    int songIndex = playlistBox->GetSelection();

    std::string Song;
    wxString wxSong;

    wxSong = playlistBox->GetString(songIndex);

    Song = std::string(wxSong);

    Song = "/home/apoorv/Music/mod/" + Song;

    std::cout << "State: " << mediaCtrl->GetState() << std::endl;
    std::cout << "Playlist: " << playlistBox->GetString(songIndex) << std::endl;
    std::cout << "Load: " << mediaCtrl->Load(Song) << std::endl;

    if (playlistBox->GetCount() != 0)
    {
        if (playlistBox->GetString(songIndex) != "")
        {
            mediaCtrl->Stop();
            playlistBox->Delete(songIndex);
        }
        else
        {
            playlistBox->Delete(songIndex);
        }
    }
    else
    {
        wxMessageBox("Playlist already empty, nothing to remove.");
    }
}

void Widget::ClearPlaylist(wxCommandEvent& event)
{
    mediaCtrl->Stop();

    playlistBox->Clear();
}
/*
void Widget::Playlist(wxCommandEvent &event)
{
    if (playlistButton->GetValue() == 1)
    {
        addTrackButton->Show();
        addFolderButton->Show();
        removeTrackButton->Show();
        clearPlaylistButton->Show();
        libraryViewButton->Show();
        playlistBox->Show();
        vboxSizer->Layout();

        this->SetSize(wxSize(500,600));
    }
    else
    {
        addTrackButton->Hide();
        addFolderButton->Hide();
        removeTrackButton->Hide();
        clearPlaylistButton->Hide();
        libraryViewButton->Hide();
        playlistBox->Hide();
        vboxSizer->Layout();

        this->SetSize(wxSize(500,210));
    }
}
*/
void Widget::Shuffle(wxCommandEvent &event)
{
    if (shuffleButton->GetValue() == 0)
    {
        shuffleButton->SetLabel("Shuffle off");
        shuffle = false;
    }
    else if (shuffleButton->GetValue() == 1)
    {
        shuffleButton->SetLabel("Shuffle on");
        shuffle = true;
    }
}

void Widget::Repeat(wxCommandEvent &event)
{
    mode_count ++;

    if (mode_count == 0)
    {
        repeatButton->SetValue(false);
        repeatButton->SetLabel("Repeat off");
        std::cout << "First: " << mode_count << std::endl;
        std::cout << "First Val: " << repeatButton->GetValue() << std::endl;
    }
    else if (mode_count == 1)
    {
        repeatButton->SetValue(true);
        repeatButton->SetLabel("Repeat one");
        std::cout << "Second: " << mode_count << std::endl;
        std::cout << "Second Val: " << repeatButton->GetValue() << std::endl;
    }
    else if (mode_count == 2)
    {
        repeatButton->SetValue(true);
        repeatButton->SetLabel("Repeat all");
        std::cout << "Third: " << mode_count << std::endl;
        std::cout << "Third Val: " << repeatButton->GetValue() << std::endl;
        mode_count = -1;
    }
}
