#pragma once

#include <wx/frame.h>
//#include <wx/simplebook.h>

#include "SmallPlayer.hpp"
#include "BigPlayer.hpp"
//#include "Widgets.hpp"

class MainFrame : public wxFrame
{
    public:
        MainFrame();
        ~MainFrame();

    private:
        enum
        {
            PageSmall = 0,
            PageLarge,
        };
/*
        enum PlayerControlIDs
        {
            PCID_SwitchUI = wxID_HIGHEST + 1,
        };
*/
        wxSimplebook* book;
/*
    public:
        SmallPlayer* sp = new SmallPlayer(book);
        BigPlayer* bp = new BigPlayer(book);
*/
    public:
//        Command* command;
        void SwitchLayout(wxCommandEvent& event);
//        void Play(wxCommandEvent& event);
};
