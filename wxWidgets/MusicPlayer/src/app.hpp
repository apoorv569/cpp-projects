#pragma once

#include <wx/app.h>
#include "MainFrame.hpp"

class App : public wxApp
{
    private:
        MainFrame* mainFrame = nullptr;

    public:
        App();
        ~App();

    public:
        virtual bool OnInit();
};
