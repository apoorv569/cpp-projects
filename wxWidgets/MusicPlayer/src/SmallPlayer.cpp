#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <type_traits>
#include "SmallPlayer.hpp"

SmallPlayer::SmallPlayer(wxWindow* window): wxPanel(window, wxID_ANY, wxDefaultPosition, wxDefaultSize)
{
    paused = false;
    stopped = false;
    muted = false;

    mode_count = 0;

    font = new wxFont(20, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false);

    timer = new wxTimer(this);
    this->Bind(wxEVT_TIMER, &SmallPlayer::UpdateElapsedTime, this);

    vboxSizer = new wxBoxSizer(wxVERTICAL);
    hboxSizer0 = new wxBoxSizer(wxHORIZONTAL);
    hboxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    hboxSizer2 = new wxBoxSizer(wxVERTICAL);
    hboxSizer3 = new wxBoxSizer(wxVERTICAL);
    hboxSizer4 = new wxBoxSizer(wxHORIZONTAL);
    hboxSizer5 = new wxBoxSizer(wxHORIZONTAL);
    hboxSizer6 = new wxBoxSizer(wxVERTICAL);
    hboxSizer7 = new wxBoxSizer(wxHORIZONTAL);
    hboxSizer8 = new wxBoxSizer(wxVERTICAL);
    hboxSizer9 = new wxBoxSizer(wxHORIZONTAL);

//    play = new wxBitmap("/home/apoorv/play.png", wxBITMAP_TYPE_PNG);

    playButton = new wxButton(this, PCID_Play, wxT("|>"), wxDefaultPosition, wxDefaultSize, 0);
//    playButton->SetBitmap(*play);
    Bind(wxEVT_BUTTON, &SmallPlayer::Play, this, PCID_Play);

    pauseButton = new wxButton(this, PCID_Pause, wxT("||"), wxDefaultPosition, wxDefaultSize, 0);
    pauseButton->Bind(wxEVT_BUTTON, &SmallPlayer::Pause, this);

    stopButton = new wxButton(this, PCID_Stop, wxT("[]"), wxDefaultPosition, wxDefaultSize, 0);
    stopButton->Bind(wxEVT_BUTTON, &SmallPlayer::Stop, this);

    previousTrackButton = new wxButton(this, PCID_PreviousTrack, wxT("<<"), wxDefaultPosition, wxDefaultSize, 0);
    previousTrackButton->Bind(wxEVT_BUTTON, &SmallPlayer::PreviousTrack, this);

    nextTrackButton = new wxButton(this, PCID_NextTrack, wxT(">>"), wxDefaultPosition, wxDefaultSize, 0);
    nextTrackButton->Bind(wxEVT_BUTTON, &SmallPlayer::NextTrack, this);

    muteButton = new wxToggleButton(this, PCID_Mute, wxT("<))"), wxDefaultPosition, wxDefaultSize, 0);
    muteButton->Bind(wxEVT_TOGGLEBUTTON, &SmallPlayer::Mute, this);

    staticText = new wxStaticText(this, PCID_StaticText, wxT("--:--/--:--"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
    staticText->SetFont(*font);

    volumeSlider = new wxSlider(this, PCID_VolumeSlider, 100, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
    volumeSlider->Connect(wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(SmallPlayer::Volume), NULL, this);

    shuffleButton = new wxToggleButton(this, PCID_Shuffle, wxT("Shuffle off"), wxDefaultPosition, wxDefaultSize, 0);
    shuffleButton->Bind(wxEVT_TOGGLEBUTTON, &SmallPlayer::Shuffle, this);

    repeatButton = new wxToggleButton(this, PCID_Repeat, wxT("Repeat off"), wxDefaultPosition, wxDefaultSize, 0);
    repeatButton->Bind(wxEVT_TOGGLEBUTTON, &SmallPlayer::Repeat, this);

    playlistButton = new wxToggleButton(this, PCID_Playlist, wxT("Playlist"), wxDefaultPosition, wxDefaultSize, 0);
    playlistButton->Bind(wxEVT_TOGGLEBUTTON, &SmallPlayer::Playlist, this);
    playlistButton->SetValue(true);

    seekBar = new wxSlider(this, PCID_SeekBar, 0, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
    seekBar->Connect(wxEVT_SCROLL_THUMBTRACK, wxScrollEventHandler(SmallPlayer::SeekBar), NULL, this);

    playlistBox = new wxListBox(this, wxLB_ALWAYS_SB);

    addTrackButton = new wxButton(this, PCID_AddTrack, wxT("+"), wxDefaultPosition, wxDefaultSize, 0);
    addTrackButton->Bind(wxEVT_BUTTON, &SmallPlayer::AddSong, this);

    addFolderButton = new wxButton(this, PCID_AddDirectory, wxT("*"), wxDefaultPosition, wxDefaultSize, 0);

    removeTrackButton = new wxButton(this, PCID_RemoveTrack, wxT("-"), wxDefaultPosition, wxDefaultSize, 0);
    removeTrackButton->Bind(wxEVT_BUTTON, &SmallPlayer::RemoveSong, this);

    clearPlaylistButton = new wxButton(this, PCID_ClearPlaylist, wxT("C"), wxDefaultPosition, wxDefaultSize, 0);
    clearPlaylistButton->Bind(wxEVT_BUTTON, &SmallPlayer::ClearPlaylist, this);

    libraryViewButton = new wxButton(this, PCID_SwitchUI, wxT("L"), wxDefaultPosition, wxDefaultSize, 0);
//    libraryViewButton->Bind(wxEVT_BUTTON, &SmallPlayer::SwitchLayout, this, PCID_SwitchUI);

    mediaCtrl = new wxMediaCtrl(this, PCID_MediaCtrl, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxMEDIABACKEND_GSTREAMER);

    hboxSizer2->Add(staticText, 1, wxALL | wxEXPAND | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer4->Add(muteButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, 5);
    hboxSizer4->Add(volumeSlider, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer3->Add(hboxSizer4, 1, wxEXPAND, 5);
    hboxSizer5->Add(shuffleButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer5->Add(repeatButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer5->Add(playlistButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer3->Add(hboxSizer5, 0, wxEXPAND, 5);
    hboxSizer1->Add(hboxSizer2, 1, wxEXPAND, 5);
    hboxSizer1->Add(hboxSizer3, 1, wxEXPAND, 5);

    vboxSizer->Add(hboxSizer1, 0, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 10);
    hboxSizer6->Add(seekBar, 1, wxALL | wxALIGN_CENTER_HORIZONTAL | wxEXPAND, 5);
    vboxSizer->Add(hboxSizer6, 0, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 10);

    hboxSizer7->Add(previousTrackButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer7->Add(playButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer7->Add(pauseButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer7->Add(stopButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer7->Add(nextTrackButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    vboxSizer->Add(hboxSizer7, 0, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 10);

    hboxSizer8->Add(playlistBox, 1, wxALL | wxALIGN_CENTER_VERTICAL | wxEXPAND, 5);
    vboxSizer->Add(hboxSizer8, 1, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 10);
    vboxSizer->Add(-1, 10);

    hboxSizer9->Add(addTrackButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer9->Add(addFolderButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer9->Add(removeTrackButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer9->Add(clearPlaylistButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    hboxSizer9->Add(libraryViewButton, 1, wxALL | wxALIGN_CENTER_VERTICAL, 5);
    vboxSizer->Add(hboxSizer9, 0, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 10);
    vboxSizer->Add(-1, 10);

    this->SetSizeHints(wxDefaultSize, wxDefaultSize);
    this->SetSizer(vboxSizer);
    this->Layout();
    this->Centre(wxBOTH);
}

void SmallPlayer::AddSong(wxCommandEvent& event)
{
    filePickerDialogBox = new wxFileDialog(this,
                                           wxFileSelectorPromptStr,
                                           wxEmptyString,
                                           wxEmptyString,
                                           "Ogg Files (*.ogg)|*.ogg | Wav Files (*.wav)|*.wav | Mp3 Files (*.mp3)|*.mp3 | All Files (*.*)|*.*",
                                           wxFD_DEFAULT_STYLE);

    if (filePickerDialogBox->ShowModal() == wxID_CANCEL)
    {
        return;
    }
    else if (filePickerDialogBox->ShowModal() == wxID_OK)
    {
        playlistBox->Append(filePickerDialogBox->GetFilename());
    }
}

void SmallPlayer::Play(wxCommandEvent& event)
{
    stopped = false;

    int songIndex;

    std::string Song;
    wxString wxSong;

    songIndex = playlistBox->GetSelection();
    wxSong = playlistBox->GetString(songIndex);
    Song = std::string(wxSong);
    Song = "/home/apoorv/Music/mod/" + Song;

    std::cout << "wxString: " << wxSong << std::endl;
    std::cout << "Song: " << Song << std::endl;
    std::cout << mode_count << std::endl;

    mediaCtrl->Load(Song);
    mediaCtrl->Play();

    std::cout << "Controls: " << mediaCtrl->ShowPlayerControls(wxMEDIACTRLPLAYERCONTROLS_DEFAULT) << std::endl;

    timer->Start(1000);
}

void SmallPlayer::Pause(wxCommandEvent& event)
{
    if (!stopped)
    {
        if (paused)
        {
            std::cout << mediaCtrl->Tell() << std::endl;
            mediaCtrl->Seek(mediaCtrl->Tell());
            mediaCtrl->Play();
            timer->Start(1000);
            paused = false;
        }
        else if (!paused)
        {
            mediaCtrl->Pause();
            timer->Stop();
            paused = true;
        }
    }
    else { }
}

void SmallPlayer::PreviousTrack(wxCommandEvent& event)
{
    int prevSongIndex = playlistBox->GetSelection()-1;

    std::string Song;
    wxString wxprevSong;

    wxprevSong = playlistBox->GetString(prevSongIndex);
    Song = std::string(wxprevSong);

    Song = "/home/apoorv/Music/mod/" + wxprevSong;

    playlistBox->SetSelection(prevSongIndex);

    mediaCtrl->Load(Song);
    mediaCtrl->Play();
}

void SmallPlayer::NextTrack(wxCommandEvent& event)
{
    int nextSongIndex = playlistBox->GetSelection()+1;

    std::string Song;
    wxString wxnextSong;

    wxnextSong = playlistBox->GetString(nextSongIndex);
    Song = std::string(wxnextSong);

    Song = "/home/apoorv/Music/mod/" + wxnextSong;

    playlistBox->SetSelection(nextSongIndex);

    mediaCtrl->Load(Song);
    mediaCtrl->Play();
}

void SmallPlayer::Stop(wxCommandEvent& event)
{
    mediaCtrl->Stop();
    timer->Stop();
    seekBar->SetValue(0);
    staticText->SetLabel("--:--/--:--");

    stopped = true;
}

void SmallPlayer::UpdateElapsedTime(wxTimerEvent& event)
{
    seekBar->SetValue(mediaCtrl->Tell());
    seekBar->SetMax(mediaCtrl->Length());

    std::string SongTime = std::to_string(mediaCtrl->Tell() / 60000) + ":" + std::to_string(mediaCtrl->Tell() / 1000)
        + "/" + std::to_string(mediaCtrl->Length() / 60000) + ":" + std::to_string(mediaCtrl->Length() / 1000);

    staticText->SetLabel(SongTime);

    if (mediaCtrl->Tell() == mediaCtrl->Length())
    {
        std::cout << "Song ended" << std::endl;
        mediaCtrl->Seek(wxFromStart);
        mediaCtrl->Play();
    }

    std::cout << "Tell: " << mediaCtrl->Tell() << std::endl;
    std::cout << "Length: " << mediaCtrl->Length() << std::endl;
}

void SmallPlayer::SeekBar(wxScrollEvent& event)
{
    mediaCtrl->Seek(seekBar->GetValue());

    std::cout << "MEDIACTRL SEEK POS: " <<  mediaCtrl->Tell() / 60000 << ":" << mediaCtrl->Tell() / 1000 << std::endl;
    std::cout << "SLIDER SEEK POS: " << seekBar->GetValue() << std::endl;
}

void SmallPlayer::Volume(wxScrollEvent& event)
{
    double getVolume = mediaCtrl->GetVolume() * 100.0;

    std::cout << "wxMediaCtrl Vol: " << getVolume << std::endl;
    std::cout << "Slider Vol: " << volumeSlider->GetValue() << std::endl;

    mediaCtrl->SetVolume(static_cast<double>(volumeSlider->GetValue()) / 100);
}

void SmallPlayer::Mute(wxCommandEvent& event)
{
    int getVolume = mediaCtrl->GetVolume() * 100;
    if (!muted)
    {
        mediaCtrl->SetVolume(0);
        muteButton->SetValue(true);
        std::cout << "muted: " << getVolume << std::endl;
        muted = true;
    }
    else if (muted)
    {
        mediaCtrl->SetVolume(1);
        muteButton->SetValue(false);
        std::cout << "unmuted: " << getVolume << std::endl;
        muted = false;
    }
}

void SmallPlayer::RemoveSong(wxCommandEvent& event)
{
    int songIndex = playlistBox->GetSelection();

    std::string Song;
    wxString wxSong;

    wxSong = playlistBox->GetString(songIndex);

    Song = std::string(wxSong);

    Song = "/home/apoorv/Music/mod/" + Song;

    std::cout << "State: " << mediaCtrl->GetState() << std::endl;
    std::cout << "Playlist: " << playlistBox->GetString(songIndex) << std::endl;
    std::cout << "Load: " << mediaCtrl->Load(Song) << std::endl;

    if (playlistBox->GetCount() != 0)
    {
        if (playlistBox->GetString(songIndex) != "")
        {
            mediaCtrl->Stop();
            playlistBox->Delete(songIndex);
        }
        else
        {
            playlistBox->Delete(songIndex);
        }
    }
    else
    {
        wxMessageBox("Playlist already empty, nothing to remove.");
    }
}

void SmallPlayer::ClearPlaylist(wxCommandEvent& event)
{
    mediaCtrl->Stop();

    playlistBox->Clear();
}

void SmallPlayer::Playlist(wxCommandEvent &event)
{
    if (playlistButton->GetValue() == 1)
    {
        addTrackButton->Show();
        addFolderButton->Show();
        removeTrackButton->Show();
        clearPlaylistButton->Show();
        libraryViewButton->Show();
        playlistBox->Show();
        vboxSizer->Layout();

        this->SetSize(wxSize(500,600));
    }
    else
    {
        addTrackButton->Hide();
        addFolderButton->Hide();
        removeTrackButton->Hide();
        clearPlaylistButton->Hide();
        libraryViewButton->Hide();
        playlistBox->Hide();
        vboxSizer->Layout();

        this->SetSize(wxSize(500,210));
    }
}

void SmallPlayer::Shuffle(wxCommandEvent &event)
{
    if (shuffleButton->GetValue() == 0)
    {
        shuffleButton->SetLabel("Shuffle off");
        shuffle = false;
    }
    else if (shuffleButton->GetValue() == 1)
    {
        shuffleButton->SetLabel("Shuffle on");
        shuffle = true;
    }
}

void SmallPlayer::Repeat(wxCommandEvent &event)
{
    mode_count ++;

    if (mode_count == 0)
    {
        repeatButton->SetValue(false);
        repeatButton->SetLabel("Repeat off");
        std::cout << "First: " << mode_count << std::endl;
        std::cout << "First Val: " << repeatButton->GetValue() << std::endl;
    }
    else if (mode_count == 1)
    {
        repeatButton->SetValue(true);
        repeatButton->SetLabel("Repeat one");
        std::cout << "Second: " << mode_count << std::endl;
        std::cout << "Second Val: " << repeatButton->GetValue() << std::endl;
    }
    else if (mode_count == 2)
    {
        repeatButton->SetValue(true);
        repeatButton->SetLabel("Repeat all");
        std::cout << "Third: " << mode_count << std::endl;
        std::cout << "Third Val: " << repeatButton->GetValue() << std::endl;
        mode_count = -1;
    }
}

SmallPlayer::~SmallPlayer(){}
