#include "app.hpp"

wxIMPLEMENT_APP(App);

bool App::OnInit()
{
    mainFrame = new MainFrame();
    mainFrame->Show(true);
    return true;
}
