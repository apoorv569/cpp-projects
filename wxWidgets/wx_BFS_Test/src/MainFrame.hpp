#pragma once

#include <deque>
#include <iostream>
#include <string>

#include <wx/wx.h>
#include <wx/dataview.h>

class MainFrame : public wxFrame
{
    public:
        MainFrame();
        ~MainFrame();

    public:
        enum ID
        {
            DVC_ID = wxID_HIGHEST + 1
        };

    public:
        wxDataViewTreeCtrl* tree;

        wxDataViewItem parent;

    public:
        void Bfs();
};
