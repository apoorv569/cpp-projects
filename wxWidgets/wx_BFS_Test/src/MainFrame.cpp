#include "MainFrame.hpp"
#include "wx/dataview.h"

MainFrame::MainFrame(): wxFrame(NULL, wxID_ANY, "wxDVC Test", wxDefaultPosition, wxSize(800, 600))
{
    tree = new wxDataViewTreeCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize);

    parent =
        tree->AppendContainer( wxDataViewItem(0), "The Root", 0 );
        tree->AppendItem( parent, "Child 1", 0 );
        tree->AppendItem( parent, "Child 2", 0 );
        tree->AppendItem( parent, "Child 3", 0 );
    
    wxDataViewItem cont =
        tree->AppendContainer( wxDataViewItem(0), "Container child", 0 );
        tree->AppendItem( cont, "Child 4", 0 );
        tree->AppendItem( cont, "Child 5", 0 );

    wxDataViewItem cont1 =
        tree->AppendContainer( wxDataViewItem(0), "Container child 1", 0 );
        tree->AppendItem( cont1, "Child 6", 0 );
        tree->AppendItem( cont1, "Child 7", 0 );

    wxDataViewItem cont2 =
        tree->AppendContainer( wxDataViewItem(0), "Container child 2", 0 );
        tree->AppendItem( cont2, "Child 8", 0 );
        tree->AppendItem( cont2, "Child 9", 0 );

    wxDataViewItem cont3 =
        tree->AppendContainer( wxDataViewItem(0), "Container child 3", 0 );
        tree->AppendItem( cont3, "Child 10", 0 );
        tree->AppendItem( cont3, "Child 11", 0 );

    wxDataViewItem cont4 =
        tree->AppendContainer( wxDataViewItem(0), "Container child 4", 0 );
        tree->AppendItem( cont4, "Child 12", 0 );
        tree->AppendItem( cont4, "Child 13", 0 );
        tree->AppendItem( cont4, "Child 14", 0 );

        Bfs();
}

void MainFrame::Bfs()
{
    std::deque<wxDataViewItem> nodes;
    int containerRow = 0;

    wxDataViewItem container = tree->GetNthChild(wxDataViewItem(0), containerRow);
    nodes.push_back(container);

    while(!nodes.empty())
    {
        wxDataViewItem currentItem = nodes.front();
        nodes.pop_front();

        wxLogDebug("Current item: %s", tree->GetItemText(currentItem));

        int row = 0;

        // Get first child
        wxDataViewItem child = tree->GetNthChild(currentItem, row);

        wxLogDebug("Child item: %s", tree->GetItemText(child));

        while (child.IsOk())
        {
            // Get next child
            row ++;

            nodes.push_back(child);
            child = tree->GetNthChild(currentItem, row);

            wxLogDebug("Next item: %s", tree->GetItemText(child));

            // check if child is container
            if (tree->IsContainer(currentItem))
            {
                wxLogDebug("Found a container..");

                containerRow ++;
                currentItem = container;
            }
        }
    }
}

MainFrame::~MainFrame(){}
