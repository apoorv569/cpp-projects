((nil . ((cmake-ide-project-dir . "~/repos/cpp-projects/wxWidgets/wx_BFS_Test")

(cmake-ide-build-dir . "~/repos/cpp-projects/wxWidgets/wx_BFS_Test/build")

(cmake-ide-cmake-opts . "-DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DPORTABLE=1 -DCMAKE_CXX_COMPILER='/usr/bin/g++'")

(projectile-project-name . "Bfs")

(projectile-project-run-cmd . "~/repos/cpp-projects/wxWidgets/wx_BFS_Test/run.sh")

(projectile-project-test-cmd . "./test.sh"))))
