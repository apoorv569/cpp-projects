<p align="center"><img src=""></p>
<h2 align="center">SampleBrowser</h2>

## What is SampleBrowser?

SampleBrowser let's you manage your samples in a nice and simple way, just add a directory where you store all your samples, or drag and drop a directory on it to add samples to it, and it will help sort, search, play and view some information about the sample. You can also drag and drop from SampleBrowser to other application(not implemented yet).

## Dependencies
On Arch based distributions,

```
sudo pacman -S wxgtk3 wxsvg sqlite taglib yaml-cpp
```

On Ubuntu and Ubuntu based systems,

```
sudo apt install libwxbase3.0-dev libwxgtk-media3.0-gtk3-dev libwxgtk3.0-gtk3-dev wx3.0-headers libwxsvg-dev libwxsvg3 libsqlite3-dev libyaml-cpp-dev libtagc0-dev libtag1-dev libtagc0
```

You might also need to install `git`, `meson` and `g++` as well, if you don't already have them installed in order to compile SampleBrowser.

## How to build SampleBrowser?

Download the source code from this repository or use a git clone:

```
git clone https://gitlab.com/apoorv569/SampleBrowser
cd SampleBrowser
meson build
ninja -C build
```
	
## How to run SampleBrowser?

To run SampleBrowser:

```
cd build
./SampleBrowser
```

## Some keybindings

## Can I configure SampleBrowser?

SampleBrowser comes with a `config.yaml` file, that you can edit to change some settings about the SampleBrowser.
