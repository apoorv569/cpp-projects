#include <string>

#include <wx/treectrl.h>
#include <wx/vector.h>
#include <wx/variant.h>

#include <sqlite3.h>

class Database
{
    public:
        Database();
        ~Database();

    private:
        // -------------------------------------------------------------------
        sqlite3* m_Database;
        int rc;
        char* m_ErrMsg;
        sqlite3_stmt* m_Stmt;

    public:
        // -------------------------------------------------------------------
        // Insert into database
        void InsertSample(int favorite, std::string filename,
                          std::string samplePack, std::string type,
                          int channels, int length, int sampleRate, int bitrate,
                          std::string path);

        // -------------------------------------------------------------------
        // Update database
        void UpdateFavoriteColumn(std::string filename, int value);
        void UpdateFolder(std::string folderName);
        void UpdateFavoriteFolderDatabase(std::string filename,
                                          std::string folderName);
        void UpdateSampleType(std::string filename, std::string type);

        // -------------------------------------------------------------------
        // Get from database
        std::string GetSampleType(std::string filename);
        int GetFavoriteColumnValueByFilename(std::string filename);
        std::string GetSamplePathByFilename(std::string filename);

        // -------------------------------------------------------------------
        // Check database
        bool HasSample(std::string filename);

        // -------------------------------------------------------------------
        // Remove from database
        void RemoveSampleFromDatabase(std::string filename);

        // -------------------------------------------------------------------
        wxVector<wxVector<wxVariant>>
        LoadDatabase(wxVector<wxVector<wxVariant>> &vecSet, wxTreeCtrl &tree,
                     wxTreeItemId &item);
        wxVector<wxVector<wxVariant>>
        FilterDatabaseBySampleName(wxVector<wxVector<wxVariant>> &sampleVec,
                                   std::string sampleName);
};
