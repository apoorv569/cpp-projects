#include "wx/defs.h"
#include "wx/gdicmn.h"

#include "MainFrame.hpp"

MainFrame::MainFrame(): wxFrame(NULL, wxID_ANY, "Sample Browser", wxDefaultPosition)
{
    int height = 600, width = 800;

    const std::string filepath = "config.yaml";
    Serializer serializer(filepath);

    height = serializer.DeserializeWinSize("Height", height);
    width = serializer.DeserializeWinSize("Width", width);

    this->SetSize(width, height);
    this->Center(wxBOTH);

    m_Browser = new Browser(this);
}

MainFrame::~MainFrame(){}
