#include "MainFrame.hpp"

MainFrame::MainFrame(): wxFrame(NULL, wxID_ANY, "wxDVC Test", wxDefaultPosition, wxSize(800, 600))
{
    dvc = new wxDataViewListCtrl(this, ID::DVC_ID, wxDefaultPosition, wxDefaultSize, wxDV_SINGLE | wxDV_HORIZ_RULES | wxDV_VERT_RULES);

    dvc->AppendToggleColumn("COL 0", wxDATAVIEW_CELL_ACTIVATABLE, 30, wxALIGN_CENTER, wxDATAVIEW_COL_RESIZABLE);
    dvc->AppendTextColumn("COL 1", wxDATAVIEW_CELL_INERT, 240, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    dvc->AppendTextColumn("COL 2", wxDATAVIEW_CELL_INERT, 140, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    dvc->AppendTextColumn("COL 3", wxDATAVIEW_CELL_INERT, 160, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    dvc->AppendTextColumn("COL 4", wxDATAVIEW_CELL_INERT, 100, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    dvc->AppendTextColumn("COL 5", wxDATAVIEW_CELL_INERT, 150, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    dvc->AppendTextColumn("COL 6", wxDATAVIEW_CELL_INERT, 70, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    dvc->AppendTextColumn("COL 7", wxDATAVIEW_CELL_INERT, 80, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);

    dvc->Bind(wxEVT_DATAVIEW_SELECTION_CHANGED, &MainFrame::OnClickDVC, this, DVC_ID);

    wxVector<wxVariant> Data;
    Data.clear();
    Data.push_back(false);
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");

    dvc->AppendItem(Data);
    dvc->AppendItem(Data);
    dvc->AppendItem(Data);
    dvc->AppendItem(Data);
    dvc->AppendItem(Data);
    dvc->AppendItem(Data);
    dvc->AppendItem(Data);
    dvc->AppendItem(Data);
    dvc->AppendItem(Data);
    dvc->AppendItem(Data);

   /* Create SQL statement */
    Sample = "CREATE TABLE IF NOT EXISTS DATA("
        "DATA0  INT     NOT NULL,"
        "DATA1  TEXT    NOT NULL,"
        "DATA2  TEXT    NOT NULL,"
        "DATA3  INT     NOT NULL,"
        "DATA4  INT     NOT NULL,"
        "DATA5  INT     NOT NULL,"
        "DATA6  INT     NOT NULL,"
        "DATA7  INT     NOT NULL,"
        "DATA8  INT     NOT NULL);";

    try
    {
        rc = sqlite3_open("Data.db", &DB);
        rc = sqlite3_exec(DB, Sample.c_str(), NULL, 0, &ErrorMessage);

        if (rc != SQLITE_OK)
        {
            std::cerr << "Error! Cannot create table." << std::endl;
            sqlite3_free(ErrorMessage);
        }
        else
        {
            std::cout << "Table created successfuly." << std::endl;
        }

        sqlite3_close(DB);
    }
    catch (const std::exception &exception)
    {
        std::cerr << exception.what();
    }

    MainFrame::InsertData(0, "Data", "Data", 1, 2, 3, 4, "Data", "DATA");
    MainFrame::InsertData(0, "Data", "Data", 1, 2, 3, 4, "Data", "DATA");
    MainFrame::InsertData(0, "Data", "Data", 1, 2, 3, 4, "Data", "DATA");
    MainFrame::InsertData(0, "Data", "Data", 1, 2, 3, 4, "Data", "DATA");
}

MainFrame::~MainFrame(){}

void MainFrame::OnClickDVC(wxDataViewEvent &event)
{
    std::cout << "Clicked DVC" << std::endl;
    std::cout.flush();
    wxLogMessage("Clicked DVC");
    wxLogDebug("Clicked DVC");
}

void MainFrame::InsertData(int data0, std::string data1,
                            std::string data2, int data3, int data4,
                            int data5, int data6, std::string data7,
                            std::string data8)
{
    try
    {
        rc = sqlite3_open("Data.db", &DB);

        sql = "INSERT INTO DATA (FAVORITE, FILENAME, SAMPLEPACK, CHANNELS, \
                                    LENGTH, SAMPLERATE, BITRATE, BITSPERSAMPLE, PATH) \
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";

        // create the prepared statement
        rc = sqlite3_prepare_v2(DB, sql.c_str(), sql.size(), &stmt, NULL);
        // error handling goes here

        rc = sqlite3_bind_int(stmt, 1, data0);
        // error handling goes here

        rc = sqlite3_bind_text(stmt, 2, data1.c_str(), data1.size(), SQLITE_STATIC);
        // error handling goes here

        rc = sqlite3_bind_text(stmt, 3, data2.c_str(), data2.size(), SQLITE_STATIC);
        // error handling goes here

        rc = sqlite3_bind_int(stmt, 4, data3);
        // error handling goes here

        rc = sqlite3_bind_int(stmt, 5, data4);
        // error handling goes here

        rc = sqlite3_bind_int(stmt, 6, data5);
        // error handling goes here

        rc = sqlite3_bind_int(stmt, 7, data6);
        // error handling goes here

        rc = sqlite3_bind_text(stmt, 8, data7.c_str(), data7.size(), SQLITE_STATIC);
        // error handling goes here

        rc = sqlite3_bind_text(stmt, 9, data8.c_str(), data8.size(), SQLITE_STATIC);
        // error handling goes here

        if (sqlite3_step(stmt) != SQLITE_DONE)
        {
            std::cout << "Not inserted data." << "\n";
        }

        rc = sqlite3_finalize(stmt);

        if (rc != SQLITE_OK)
        {
            std::cerr << "Error! Cannot insert data into table." << std::endl;
            sqlite3_free(ErrorMessage);
        }
        else if (rc == SQLITE_BUSY)
        {
            std::cout << "BUSY" << std::endl;
        }
        else if (rc == SQLITE_DONE)
        {
            std::cout << "DONE" << std::endl;
        }
        else if (rc == SQLITE_ERROR)
        {
            std::cout << "ERROR" << std::endl;
        }
        else if (rc == SQLITE_MISUSE)
        {
            std::cout << "MISUSE" << std::endl;
        }
        else
        {
            std::cout << "Data inserted successfully." << ErrorMessage << std::endl;
        }

        sqlite3_close(DB);
    }
    catch (const std::exception &exception)
    {
        std::cerr << exception.what();
    }
}
