#pragma once

#include <wx/wx.h>

#include "MainFrame.hpp"

class App : public wxApp
{
    private:
        MainFrame* mainFrame = nullptr;

    public:
        virtual bool OnInit();
};
