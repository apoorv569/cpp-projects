#pragma once

#include <wx/wx.h>
#include <wx/dataview.h>

#include <sqlite3.h>

class MainFrame : public wxFrame
{
    public:
        MainFrame();
        ~MainFrame();

    public:
        enum ID
        {
            DVC_ID = wxID_HIGHEST + 1
        };

    public:
        sqlite3* DB;
        int rc;
        char* ErrorMessage;
        std::string Sample;
        std::string sql;

        sqlite3_stmt* stmt;

    public:
        void InsertData(int data0, std::string data1,
                          std::string data2, int data3,
                          int data4, int data5, int data6,
                          std::string data7, std::string data8);

    public:
        wxDataViewListCtrl* dvc;

    public:
        void OnClickDVC(wxDataViewEvent& event);
};
