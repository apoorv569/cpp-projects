#include "ControlID_Enums.hpp"
#include "Browser.hpp"

Browser::Browser()
    : wxFrame(NULL, wxID_ANY, "wxAccelTest", wxDefaultPosition, wxSize(800, 600))
{
    // Initializing to start as false
    autoplay = false;
    muted = false;
    loop = false;
    stopped = false;

    // Initializing BoxSizers
    TopSizer = new wxBoxSizer(wxVERTICAL);

    BottomLeftPanelMainSizer = new wxBoxSizer(wxVERTICAL);
    TopPanelMainSizer = new wxBoxSizer(wxVERTICAL);
    BottomRightPanelMainSizer = new wxBoxSizer(wxVERTICAL);

    SearchBoxSizer = new wxBoxSizer(wxHORIZONTAL);
    ListCtrlSizer = new wxBoxSizer(wxVERTICAL);

    BrowserControlSizer = new wxBoxSizer(wxHORIZONTAL);

    wxPanel* mPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize);

    // Creating top splitter window,
    TopSplitter = new wxSplitterWindow(mPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_BORDER | wxSP_LIVE_UPDATE, _T("SPLIT TOP"));
    TopSplitter->SetMinimumPaneSize(200);
    TopSplitter->SetSashGravity(0);

    // Top half of right splitter window,
    TopPanel = new wxPanel(TopSplitter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("SPLIT TOP RIGHT"));

    // Creating right half of the top splitter window,
    BottomSplitter = new wxSplitterWindow(TopSplitter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_BORDER | wxSP_LIVE_UPDATE, _T("SPLIT BOTTOM"));
    BottomSplitter->SetMinimumPaneSize(100);
    BottomSplitter->SetSashGravity(0.2);

    // Left half of the top splitter window,
    BottomLeftPanel = new wxPanel(BottomSplitter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("MANAGER"));

    // Initializing wxNotebook
    ViewChoice = new wxNotebook(BottomLeftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, _T("NOTEBOOK"));

    // Initializing wxGenericDirCtrl as one of the wxNotebook page.
    DirCtrl = new wxDirCtrl(ViewChoice,
                            BCID_DirCtrl,
                            wxDirDialogDefaultFolderStr,
                            wxDefaultPosition,
                            wxDefaultSize,
                            wxDIRCTRL_3D_INTERNAL | wxSUNKEN_BORDER,
                            wxEmptyString, 0);

    wxString path = wxStandardPaths::Get().GetDocumentsDir();
    DirCtrl->SetPath(path);

    // Initializing wxTreeCtrl as another page of wxNotebook
    CollectionView = new wxTreeCtrl(ViewChoice, BCID_CollectionView, wxDefaultPosition, wxDefaultSize, wxTR_HAS_BUTTONS | wxTR_HIDE_ROOT);

    // Adding Root Node to wxTreeCtrl
    rootNode = CollectionView->AddRoot("ROOT");

    // Adding the pages to wxNotebook
    ViewChoice->AddPage(DirCtrl, "Browse", false);
    ViewChoice->AddPage(CollectionView, "Collection", false);

    // Bottom half of right splitter window,
    BottomRightPanel = new wxPanel(BottomSplitter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D, _T("SPLIT BOTTOM RIGHT"));

    // Setting which splitter window
    TopSplitter->SplitHorizontally(TopPanel, BottomSplitter);
    BottomSplitter->SplitVertically(BottomLeftPanel, BottomRightPanel);

    // Initializing browser controls on top panel.
    AutoPlayCheck = new wxCheckBox(TopPanel, BCID_Autoplay, "Autoplay", wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
    AutoPlayCheck->SetToolTip("Autoplay");
    VolumeSlider = new wxSlider(TopPanel, BCID_Volume, 100, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
    VolumeSlider->SetToolTip("Volume");

    PlayButton = new wxButton(TopPanel, BCID_Play, "Play", wxDefaultPosition, wxDefaultSize, 0);
    PlayButton->SetToolTip("Play");
    LoopButton = new wxToggleButton(TopPanel, BCID_Loop, "Loop", wxDefaultPosition, wxDefaultSize, 0);
    LoopButton->SetToolTip("Loop");
    StopButton = new wxButton(TopPanel, BCID_Stop, "Stop", wxDefaultPosition, wxDefaultSize, 0);
    StopButton->SetToolTip("Stop");
    SettingsButton = new wxButton(TopPanel, BCID_Settings, "Settings", wxDefaultPosition, wxDefaultSize, 0);
    SettingsButton->SetToolTip("Settings");
    MuteButton = new wxToggleButton(TopPanel, BCID_Mute, "Mute", wxDefaultPosition, wxDefaultSize, 0);
    MuteButton->SetToolTip("Mute");

    // Initializing wxSearchCtrl on bottom panel.
    SearchBox = new wxSearchCtrl(BottomRightPanel,
                                 BCID_Search,
                                 "Search for samples..",
                                 wxDefaultPosition,
                                 wxDefaultSize,
                                 wxTE_PROCESS_ENTER);

    SearchBox->ShowCancelButton(true);

    // Initializing wxDataViewListCtrl on bottom panel.
    SampleListView = new wxDataViewListCtrl(BottomRightPanel,
                                            BCID_SampleListView,
                                            wxDefaultPosition,
                                            wxDefaultSize,
                                            wxDV_SINGLE |
                                            wxDV_HORIZ_RULES |
                                            wxDV_VERT_RULES);

    // Adding columns to wxDataViewListCtrl.
    SampleListView->AppendToggleColumn("", wxDATAVIEW_CELL_ACTIVATABLE, 30, wxALIGN_CENTER, wxDATAVIEW_COL_RESIZABLE);
    SampleListView->AppendTextColumn("Filename", wxDATAVIEW_CELL_INERT, 320, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    SampleListView->AppendTextColumn("Sample Pack", wxDATAVIEW_CELL_INERT, 200, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    SampleListView->AppendTextColumn("Channels", wxDATAVIEW_CELL_INERT, 100, wxALIGN_RIGHT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    SampleListView->AppendTextColumn("Length", wxDATAVIEW_CELL_INERT, 100, wxALIGN_RIGHT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    SampleListView->AppendTextColumn("Sample Rate", wxDATAVIEW_CELL_INERT, 140, wxALIGN_RIGHT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    SampleListView->AppendTextColumn("Bitrate", wxDATAVIEW_CELL_INERT, 100, wxALIGN_RIGHT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);
    SampleListView->AppendTextColumn("Bits per sample", wxDATAVIEW_CELL_INERT, 80, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE | wxDATAVIEW_COL_SORTABLE);

    wxVector<wxVariant> Data;
    Data.clear();
    Data.push_back(false);
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");
    Data.push_back("Data");

    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);
    SampleListView->AppendItem(Data);

    // Enable SampleListView to accept files to be dropped on it
    SampleListView->DragAcceptFiles(true);

    // Enable dragging a file from SampleListView
    SampleListView->EnableDragSource(wxDF_FILENAME);

    // Initialize wxInfoBar for showing information inside application
    InfoBar = new wxInfoBar(BottomRightPanel);

    // Initializing wxMediaCtrl.
    MediaCtrl = new wxMediaCtrl(mPanel, BCID_MediaCtrl, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxEmptyString);

    // Binding events.
    Bind(wxEVT_BUTTON, &Browser::OnClickPlay, this, BCID_Play);
    Bind(wxEVT_TOGGLEBUTTON, &Browser::OnClickLoop, this, BCID_Loop);
    Bind(wxEVT_BUTTON, &Browser::OnClickStop, this, BCID_Stop);
    Bind(wxEVT_TOGGLEBUTTON, &Browser::OnClickMute, this, BCID_Mute);
    Bind(wxEVT_CHECKBOX, &Browser::OnCheckAutoplay, this, BCID_Autoplay);
    Bind(wxEVT_SCROLL_THUMBTRACK, &Browser::OnSlideVolume, this, BCID_Volume);

    wxAcceleratorEntry entries[5];
    entries[0].Set(wxACCEL_NORMAL, WXK_SPACE, BCID_Play);
    entries[1].Set(wxACCEL_NORMAL, (int) 'L', BCID_Loop);
    entries[2].Set(wxACCEL_NORMAL, (int) 'S', BCID_Stop);
    entries[3].Set(wxACCEL_NORMAL, (int) 'M', BCID_Mute);
    entries[4].Set(wxACCEL_NORMAL, (int) 'P', BCID_Settings);

    wxAcceleratorTable accel(5, entries);
    this->SetAcceleratorTable(accel);

    // Adding widgets to their sizers,
    TopSizer->Add(TopSplitter, 1, wxALL | wxEXPAND, 2);

    BrowserControlSizer->Add(PlayButton, 0, wxALL | wxALIGN_LEFT, 2);
    BrowserControlSizer->Add(LoopButton, 0, wxALL | wxALIGN_LEFT, 2);
    BrowserControlSizer->Add(StopButton, 0, wxALL | wxALIGN_LEFT, 2);
    BrowserControlSizer->Add(SettingsButton, 0, wxALL | wxALIGN_LEFT, 2);
    BrowserControlSizer->Add(0,0,10, wxALL | wxEXPAND, 0);
    BrowserControlSizer->Add(MuteButton, 0, wxALL | wxALIGN_RIGHT, 2);
    BrowserControlSizer->Add(VolumeSlider, 1, wxALL | wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 2);
    BrowserControlSizer->Add(AutoPlayCheck, 0, wxALL | wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL, 2);

    TopPanelMainSizer->Add(BrowserControlSizer, 0, wxALL | wxEXPAND, 2);

    BottomLeftPanelMainSizer->Add(ViewChoice, 1, wxALL | wxEXPAND, 2);

    BottomRightPanelMainSizer->Add(SearchBox, 1, wxALL | wxEXPAND, 2);
    BottomRightPanelMainSizer->Add(InfoBar, 1, wxALL | wxEXPAND, 2);
    BottomRightPanelMainSizer->Add(SampleListView, 9, wxALL | wxEXPAND, 2);

    mPanel->SetSizer(TopSizer);
    TopSizer->Fit(mPanel);
    TopSizer->SetSizeHints(mPanel);
    TopSizer->Layout();

    // Setting which sizer the browser controls and waveform view panel should use,
    TopPanel->SetSizer(TopPanelMainSizer);
    TopPanelMainSizer->Fit(TopPanel);
    TopPanelMainSizer->SetSizeHints(TopPanel);
    TopPanelMainSizer->Layout();

    // Setting which sizer the ViewChoice panel should use,
    BottomLeftPanel->SetSizer(BottomLeftPanelMainSizer);
    BottomLeftPanelMainSizer->Fit(BottomLeftPanel);
    BottomLeftPanelMainSizer->SetSizeHints(BottomLeftPanel);
    BottomLeftPanelMainSizer->Layout();

    // Setting which sizer the ListCtrl and SearchBox panel should use,
    BottomRightPanel->SetSizer(BottomRightPanelMainSizer);
    BottomRightPanelMainSizer->Fit(BottomRightPanel);
    BottomRightPanelMainSizer->SetSizeHints(BottomRightPanel);
    BottomRightPanelMainSizer->Layout();
}

void Browser::OnClickPlay(wxCommandEvent& event)
{
    wxLogDebug("Clicked play");
    stopped = false;

    int selectedRow = SampleListView->GetSelectedRow();

    if (selectedRow < 0)
    {
        return;
    }

    wxString selection = SampleListView->GetTextValue(selectedRow, 1);

    wxLogInfo("Selected: %s", selection);
}

void Browser::OnClickLoop(wxCommandEvent& event)
{
    wxLogDebug("Clicked loop");
    if (LoopButton->GetValue())
    {
        loop = true;
    }
    else
    {
        loop = false;
    }
}

void Browser::OnClickStop(wxCommandEvent& event)
{
    wxLogDebug("Clicked stop");
    MediaCtrl->Stop();
    stopped = true;
}

void Browser::OnClickMute(wxCommandEvent& event)
{
    wxLogDebug("Clicked mute");
    if (MuteButton->GetValue())
    {
        MediaCtrl->SetVolume(0.0);
        muted = true;
    }
    else
    {
        MediaCtrl->SetVolume(1.0);
        muted = false;
    }
}

void Browser::OnCheckAutoplay(wxCommandEvent& event)
{
    wxLogDebug("Clicked autoplay");
    if (AutoPlayCheck->GetValue())
    {
        autoplay = true;
    }
    else
    {
        autoplay = false;
    }
}

void Browser::OnSlideVolume(wxScrollEvent& event)
{
    wxLogDebug("Clicked volume");
    float getVolume = MediaCtrl->GetVolume() * 100.0;

    wxLogInfo("wxMediaCtrl Vol: %d", getVolume);
    wxLogInfo("Slider Vol: %d", VolumeSlider->GetValue());

    MediaCtrl->SetVolume(float(VolumeSlider->GetValue()) / 100);
}

Browser::~Browser(){}
