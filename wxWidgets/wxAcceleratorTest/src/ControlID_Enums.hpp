#include <wx/defs.h>

enum ControlIDs
{
    // Browser controls
    BCID_Play = wxID_HIGHEST + 1,
    BCID_Settings,
    BCID_Loop,
    BCID_Stop,
    BCID_Mute,
    BCID_Autoplay,
    BCID_Volume,
    BCID_SamplePosition,
    BCID_CollectionView,
    BCID_DirCtrl,
    BCID_SampleListView,
    BCID_Search,
    BCID_MediaCtrl,
    BCID_CollectionViewAdd,
    BCID_CollectionViewRemove,

    // Setting dialog controls
    SD_BrowseConfigDir,
    SD_BrowseDatabaseDir,
    SD_AutoImport,
    SD_BrowseAutoImportDir,
    SD_FontType,
    SD_FontSize,
    SD_FontBrowseButton,

    // Menu items
    MN_FavoriteSample,
    MN_DeleteSample,
    MN_HideSample,
    MN_EditTagSample,

    // Edit tags dialog controls
    ET_ArtistCheck,
    ET_AlbumCheck,
    ET_GenreCheck,
    ET_CommentsCheck,
    ET_TypeCheck,
    ET_CustomTag,
};
