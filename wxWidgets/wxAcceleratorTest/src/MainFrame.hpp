#pragma once

#include <wx/defs.h>
#include <wx/frame.h>

#include "Browser.hpp"

class MainFrame : public wxFrame
{
    public:
        MainFrame();
        ~MainFrame();

    public:
        Browser* browser;
};
