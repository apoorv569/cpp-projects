#pragma once

#include <deque>
#include <exception>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include <cstddef>

#include <wx/accel.h>       //  <--
#include <wx/arrstr.h>      //  <--
#include <wx/busyinfo.h>    //  <--
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/collpane.h>
#include <wx/dataview.h>
#include <wx/defs.h>        //  <--
#include <wx/dir.h>         //  <--
#include <wx/dirctrl.h>
// #include <wx/dirdlg.h>
// #include <wx/dnd.h>
#include <wx/event.h>
// #include <wx/filefn.h>
// #include <wx/fileconf.h>
//#include <wx/font.h>
#include <wx/frame.h>
#include <wx/gdicmn.h>       //  <--
#include <wx/infobar.h>
#include <wx/listctrl.h>
#include <wx/log.h>          //  <--
#include <wx/mediactrl.h>
#include <wx/menu.h>         //  <--
#include <wx/msgdlg.h>       //  <--
#include <wx/notebook.h>
#include <wx/panel.h>
#include <wx/progdlg.h>      //  <--
#include <wx/scrolwin.h>
#include <wx/setup.h>
#include <wx/srchctrl.h>
#include <wx/sizer.h>
#include <wx/slider.h>
#include <wx/splitter.h>
// #include <wx/statbmp.h>
#include <wx/stdpaths.h>     //  <--
#include <wx/string.h>
#include <wx/stringimpl.h>
// #include <wx/textentry.h>
#include <wx/tglbtn.h>
#include <wx/timer.h>
#include <wx/toplevel.h>
#include <wx/treebase.h>
#include <wx/treectrl.h>
#include <wx/utils.h>
#include <wx/variant.h>
#include <wx/vector.h>
#include <wx/window.h>

#include <wxSVG/svgctrl.h>
#include <wxSVG/SVGDocument.h>

class Browser : public wxFrame
{
    public:
        Browser();
        ~Browser();

    private:
        // -------------------------------------------------------------------
        // Splitter windows
        wxSplitterWindow* TopSplitter;
        wxSplitterWindow* BottomSplitter;

        // -------------------------------------------------------------------
        // Top panel controls
        wxPanel* TopPanel;
        wxBoxSizer* TopSizer;
        wxBoxSizer* TopPanelMainSizer;
        wxBoxSizer* WaveformDisplaySizer;
        wxSVGCtrl* WaveformViewer;
        wxBoxSizer* BrowserControlSizer;
        wxButton* PlayButton;
        wxToggleButton* LoopButton;
        wxButton* StopButton;
        wxButton* SettingsButton;
        wxToggleButton* MuteButton;
        wxStaticText* SamplePosition;
        wxSlider* VolumeSlider;
        wxCheckBox* AutoPlayCheck;

        // -------------------------------------------------------------------
        // Left panel controls
        wxPanel* BottomLeftPanel;
        // wxPanel* CollectionViewPanel;
        wxBoxSizer* BottomLeftPanelMainSizer;
        // wxBoxSizer* NotebookPanelSizer;
        // wxBoxSizer* CollectionViewSizer;
        // wxBoxSizer* NotebookPanelBottomSizer;
        wxNotebook* ViewChoice;
        wxDirCtrl* DirCtrl;
        wxTreeCtrl* CollectionView;
        wxTreeItemId rootNode;
        // wxCollapsiblePane* TrashPane;
        // wxTreeCtrl* TrashedItems;
        // wxButton* AddTreeItemButton;
        // wxButton* RemoveTreeItemButton;
        // wxButton* TrashButton;

        // -------------------------------------------------------------------
        // Right panel controls
        wxPanel* BottomRightPanel;
        wxBoxSizer* BottomRightPanelMainSizer;
        wxBoxSizer* SearchBoxSizer;
        wxBoxSizer* ListCtrlSizer;
        wxSearchCtrl* SearchBox;
        wxInfoBar* InfoBar;
        wxDataViewListCtrl* SampleListView;

        // -------------------------------------------------------------------
        // MediaCtrl
        wxMediaCtrl* MediaCtrl;

    private:
        // -------------------------------------------------------------------
        bool autoplay;
        bool loop;
        bool muted;
        bool stopped;

    private:
        // -------------------------------------------------------------------
        // Top panel control handlers
        void OnClickPlay(wxCommandEvent& event);
        void OnClickLoop(wxCommandEvent& event);
        void OnClickStop(wxCommandEvent& event);
        void OnClickMute(wxCommandEvent& event);
        void OnMediaFinished(wxMediaEvent& event);
        void OnCheckAutoplay(wxCommandEvent& event);
        void OnSlideVolume(wxScrollEvent& event);
        void OnClickSettings(wxCommandEvent& event);

        // -------------------------------------------------------------------
        // DirCtrl event handlers
        void OnClickDirCtrl(wxCommandEvent& event);
        void OnDragFromDirCtrl(wxTreeEvent& event);

        // -------------------------------------------------------------------
        // SearchCtrl event handlers
        void OnDoSearch(wxCommandEvent& event);
        void OnCancelSearch(wxCommandEvent& event);

        // -------------------------------------------------------------------
        // SampleListView event handlers
        void OnCheckFavorite(wxDataViewEvent& event);
        void OnClickSampleView(wxDataViewEvent& event);
        void OnDragAndDropToSampleListView(wxDropFilesEvent& event);
        void OnDragFromSampleView(wxDataViewEvent& event);
        void OnShowSampleListViewContextMenu(wxDataViewEvent& event);
        void OnSampleListViewContextMenuSelect(wxCommandEvent& event);
};
