#pragma once

#include <wx/app.h>

#include "Browser.hpp"

class App : public wxApp
{
    public:
        App();
        ~App();

    private:
        Browser* mainFrame = nullptr;

    public:
        virtual bool OnInit();
};
