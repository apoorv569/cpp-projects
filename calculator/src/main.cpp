#include <iostream>

double addition(double a, double b){
    return a + b;
}

double subtraction(double a, double b){
    return a - b;
}

double multiplication(double a, double b){
    return a * b;
}

double division(double a, double b){
    return a / b;
}

int main(){

    double num1, num2, result;
    char op;

while (true) {

    std::cout << "Enter first number: ";
    std::cin >> num1;
    std::cout << "Enter second number: ";
    std::cin >> num2;
    std::cout << "Do you want to add, sub, mul, div? ('+', '-', '*', '/'): ";
    std::cin >> op;

    switch (op) {
        case '+':
            result = addition(num1, num2);
            break;
        case '-':
            result = subtraction(num1, num2);
            break;
        case '*':
            result = addition(num1, num2);
            break;
        case '/':
            result = addition(num1, num2);
            break;
        default:
            std::cout << "Invalid operator! Please select a valid operator.";
    }

    std::cout << result << std::endl;
}

}
