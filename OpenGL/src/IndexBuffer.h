#pragma once

class IndexBuffer
{
    public:
        IndexBuffer(const unsigned int* data, unsigned int count);
        ~IndexBuffer();

    private:
        unsigned int m_RendererID;
        unsigned int m_Count;

    public:
        void Bind() const;
        void UnBind() const;

        inline unsigned int GetCount() const { return m_Count; }
};
