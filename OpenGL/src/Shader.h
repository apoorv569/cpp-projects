#pragma once

#include <string>
#include <unordered_map>

#include <GL/glew.h>

#include <glm/glm.hpp>

struct ShaderProgramSource
{
    std::string VertexSource;
    std::string FragmentSource;
};

class Shader
{
    public:
        Shader(const std::string& filepath);
        ~Shader();

    private:
        std::string m_FilePath;
        unsigned int m_RendererID;
        mutable std::unordered_map<std::string, GLint> m_UniformLocationCache;

    public:
        void Bind() const;
        void UnBind() const;

        // Set uniforms
        void SetUniform1i(const std::string& name, int value);
        void SetUniform1f(const std::string& name, float value);
        void SetUniform2f(const std::string& name, const glm::vec2& value);
        void SetUniform3f(const std::string& name, const glm::vec3& value);
        void SetUniform4f(const std::string& name, const glm::vec4& value);

        void SetUniformMat3f(const std::string& name, const glm::mat3& matrix);
        void SetUniformMat4f(const std::string& name, const glm::mat4& matrix);

    private:
        ShaderProgramSource ParseShader(const std::string& filepath);
        unsigned int CompileShader(unsigned int type, const std::string& source);
        unsigned int CreateShader(const std::string& vertexShader, const std::string& fragmentShader);

        GLint GetUniformLocation(const std::string& name) const;
};
