#pragma once

#include "VertexBuffer.h"

class VertexBufferLayout;

class VertexArray
{
    public:
        VertexArray();
        ~VertexArray();

    private:
        unsigned int m_RendererID;

    public:
        void AddBuffer(const VertexBuffer& vb, const VertexBufferLayout& layout);

        void Bind() const;
        void UnBind() const;
};
