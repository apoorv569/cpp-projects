#pragma once

class VertexBuffer
{
    public:
        VertexBuffer(const void* data, unsigned int size);
        ~VertexBuffer();

    private:
        unsigned int m_RendererID;

    public:
        void Bind() const;
        void UnBind() const;
};
