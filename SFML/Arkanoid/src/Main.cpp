#include <iostream>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Window/WindowStyle.hpp>

bool isCollide(sf::Sprite sprite1, sf::Sprite sprite2)
{
    return sprite1.getGlobalBounds().intersects(sprite2.getGlobalBounds());
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(640, 480), "Arkanoid", sf::Style::Default);
    window.setFramerateLimit(60);

    sf::Texture orangeBlockTexture, redBlockTexture, greenBlockTexture,
        purpleBlockTexture, magentaBlockTexture, cyanBlockTexture,
        yellowBlockTexture, pinkBlockTexture, redBallTexture, greenBallTexture,
        purpleBallTexture, yellowBallTexture, smallPaddleTexture,
        mediumPaddleTexture, largePaddleTexture;

    redBallTexture.loadFromFile("/home/apoorv/repos/cpp-projects/SFML/Arkanoid/Textures/ball.png");
    largePaddleTexture.loadFromFile("/home/apoorv/repos/cpp-projects/SFML/Arkanoid/Textures/paddle.png");
    redBlockTexture.loadFromFile("/home/apoorv/repos/cpp-projects/SFML/Arkanoid/Textures/block.png");

    sf::Sprite ball(redBallTexture), paddle(largePaddleTexture), block[1000];

    paddle.setPosition(320, 460);
    ball.setPosition(320, 420);

    int n = 0;
    for (int r = 1; r <= 14; r++)
    {
        for (int c = 1; c <= 14; c++)
        {
            block[n].setTexture(redBlockTexture);
            block[n].setPosition(r*40, c*20);
            n++;
        }
    }

    float dx = 6.0f, dy = 5.0f;

    while (window.isOpen())
    {
        sf::Event evt;

        while (window.pollEvent(evt))
        {
            if (evt.type == sf::Event::Closed)
            {
                window.close();
            }
        }

        ball.move(dx, 0);
        for (int i = 0; i < n; i++)
        {
            if (isCollide(ball, block[i]))
            {
                block[i].setPosition(-100, 0);
                dx = -dx;
            }
        }

        ball.move(0, dy);
        for (int i = 0; i < n; i++)
        {
            if (isCollide(ball, block[i]))
            {
                block[i].setPosition(-100, 0);
                dy = -dy;
            }
        }

        sf::Vector2f ballPos = ball.getPosition();

        if (ballPos.x < 0 || ballPos.x > 640)
        {
            dx = -dx;
        }

        if (ballPos.y < 0 || ballPos.y > 480)
        {
            dy = -dy;
        }

        sf::Vector2f paddlePos = paddle.getPosition();

        if (paddlePos.x < 0 || paddlePos.x > 640)
        {
            dx = -dx;
        }

        if (paddlePos.y < 0 || paddlePos.y > 480)
        {
            dy = -dy;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            paddle.move(6.0f, 0.0f);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            paddle.move(-6.0f, 0.0f);
        }

        if (isCollide(paddle, ball))
        {
            dy = -10;
        }

        window.clear();
        window.draw(paddle);
        window.draw(ball);

        for (int i = 0; i < n; i++)
        {
            window.draw(block[i]);
        }

        window.display();
    }

    return 0;
}
