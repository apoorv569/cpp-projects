#pragma once

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Vector2.hpp>

class Collider
{
    public:
        Collider(sf::RectangleShape& body);
        ~Collider();

    private:
        sf::RectangleShape& body;

    public:
        bool CheckCollision(Collider& other, float push);

        sf::Vector2f GetPosition() { return body.getPosition(); }
        sf::Vector2f GetHalfSize() { return body.getSize() / 2.0f; }

        void Move(float dx, float dy) { body.move(dx, dy); }
};
