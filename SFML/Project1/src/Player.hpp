#pragma once

#include "Animation.hpp"

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

class Player
{
    public:
        Player(sf::Texture* texture, sf::Vector2u imageCount, float switchTime, float speed);
        ~Player();

    private:
        sf::RectangleShape body;
        Animation animation;
        unsigned int row;
        float speed;
        bool faceRight;

    public:
        void Update(float deltaTime);
        void Draw(sf::RenderWindow& window);
};
