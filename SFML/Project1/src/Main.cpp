#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/WindowStyle.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>

#include "Player.hpp"

int main()
{
    sf::RenderWindow window(sf::VideoMode(640, 480), "Project1", sf::Style::Default);

    sf::Texture playerTexture;
    playerTexture.loadFromFile("/home/apoorv/repos/cpp-projects/SFML/Project1/Textures/tux_from_linux.png");

    Player player(&playerTexture, sf::Vector2u(3, 9), 0.3f, 100.0f);

    float deltaTime = 0.0f;
    sf::Clock clock;

    while (window.isOpen())
    {
        deltaTime = clock.restart().asSeconds();

        sf::Event Evt;
        while (window.pollEvent(Evt))
        {
            switch (Evt.type)
            {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::Resized:
                    std::cout << "Width: " << Evt.size.width << " Height: " << Evt.size.height << std::endl;
                    break;
                case sf::Event::TextEntered:
                    std::cout << Evt.text.unicode << std::endl;
            }
        }

        player.Update(deltaTime);

        window.clear(sf::Color(150, 150, 150));
        player.Draw(window);
        window.display();
    }

    return 0;
}
