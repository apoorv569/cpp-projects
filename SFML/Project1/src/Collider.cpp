#include "Collider.hpp"

Collider::Collider(sf::RectangleShape& body):
    body(body)
{

}

Collider::~Collider()
{

}

bool Collider::CheckCollision(Collider& other, float push)
{
    sf::Vector2f otherPosition = other.GetPosition();
    sf::Vector2f otherHalfSize = other.GetHalfSize();
    sf::Vector2f thisPosition = GetPosition();
    sf::Vector2f thisHalfSize = GetHalfSize();

    return false;
}
