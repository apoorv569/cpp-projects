((nil . ((cmake-ide-project-dir . "~/repos/cpp-projects/SFML/Platformer")

(cmake-ide-build-dir . "~/repos/cpp-projects/SFML/Platformer/build")

(cmake-ide-cmake-opts . "-DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DPORTABLE=1 -DCMAKE_CXX_COMPILER='/usr/bin/g++'")

(projectile-project-name . "Platformer")

(projectile-project-run-cmd . "~/repos/cpp-projects/SFML/Platformer/run.sh")

(projectile-project-test-cmd . "./test.sh"))))
