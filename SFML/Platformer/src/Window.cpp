#include "Window.hpp"

Window::Window()
{
    gameWindow.create(sf::VideoMode(640, 480), "Platformer", sf::Style::Default);
    this->gameWindow.setFramerateLimit(60);
}

Window::~Window()
{

}

void Window::catchEvents()
{
    deltaTime = clock.restart().asSeconds();

    // Polling Window events
    while (this->gameWindow.pollEvent(this->evt))
    {
        if (this->evt.type == sf::Event::Closed)
        {
            this->gameWindow.close();
        }
    }

    player.updateMovement(this->gameWindow);
    player.updateAnimations(deltaTime);
    // player.Update(deltaTime);
}

void Window::render()
{
    // Clear the previous frame
    this->gameWindow.clear(sf::Color(150, 150, 150));

    // Render the player
    player.render(this->gameWindow);

    // Display the new frame
    this->gameWindow.display();
}

// void Window::update()
// {
//     deltaTime = 0.0f;
//     deltaTime = clock.restart().asSeconds();
// }

bool Window::isRunning()
{
    return this->gameWindow.isOpen();
}
