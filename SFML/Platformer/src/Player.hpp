#pragma once

#include "Animation.hpp"
#include "Collision.hpp"

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>

class Player
{
    public:
        Player();
        ~Player();

    private:
        sf::Sprite character;
        sf::Texture textureSheet;
        sf::IntRect currentFrame;

        sf::Sprite bgSprite;
        sf::Texture bgTexture;

        sf::Clock clock;
        float deltaTime;
        float speed;
        sf::Vector2f velocity;
        unsigned int row = 0;
        bool isMoving;
        bool canJump;
        bool canMove;
        float jumpHeight;

        Animation* animation;
        Collision collision;

    public:
        void updateMovement(sf::RenderWindow& window);
        void updateAnimations(float deltaTime);
        // void Update(float deltaTime);
        void render(sf::RenderTarget& target);
        bool onCollideWindow(sf::RenderWindow& window);
};
