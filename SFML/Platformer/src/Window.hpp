#pragma once

#include "Player.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Window/WindowStyle.hpp>

class Window
{
    public:
        Window();
        ~Window();

    private:
        sf::RenderWindow gameWindow;
        sf::Event evt;
        sf::Clock clock;

        float deltaTime;

    public:
        Player player;

    public:
        bool isRunning();
        void catchEvents();
        void update();
        void render();
};
