#include "Window.hpp"

int main()
{
    Window game;

    while (game.isRunning())
    {
        game.catchEvents();
        game.render();
    }

    return 0;
}
