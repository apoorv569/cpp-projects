#include <iostream>
#include <math.h>

#include "Player.hpp"

Player::Player()
{
    if (!this->textureSheet.loadFromFile("/home/apoorv/repos/cpp-projects/SFML/Platformer/Textures/wizard.png"))
    {
        std::cout << "ERROR! Cannot load texture file!" << "\n";
    }

    this->character.setTexture(this->textureSheet);
    this->character.setPosition(0.0f, 390.0f);
    this->character.setOrigin(character.getScale() / 2.0f);

    this->currentFrame = sf::IntRect(0, 0, 60, 90);
    // this->setTextureRect(this->currentFrame);

    isMoving = false;

    animation = new Animation(&this->textureSheet, sf::Vector2u(8, 5), 0.1f);

    if(!bgTexture.loadFromFile("/home/apoorv/repos/cpp-projects/SFML/Platformer/Textures/bg.png"))
    {
        std::cout << "ERROR! Cannot load texture file!" << "\n";
    }
    bgSprite.setTexture(bgTexture);
    bgSprite.setOrigin((float)bgTexture.getSize().x / 2, (float)bgTexture.getSize().y / 2);
    bgSprite.scale(2.5f, 2.0f);
}

Player::~Player()
{

}

bool Player::onCollideWindow(sf::RenderWindow& window)
{
    float characterPosX = character.getPosition().x;
    float characterPosY = character.getPosition().y;

    float windowWidth = (float)window.getSize().x;
    float windowHeight = (float)window.getSize().y;

    std::cout << windowWidth << " " << windowHeight << "\n";
    std::cout << characterPosX << " " << characterPosY << "\n";

    if (characterPosX > 580 || characterPosY > 390)
    {
        return true;
    }

    return false;
}

void Player::updateMovement(sf::RenderWindow& window)
{
    velocity.x = 0.0f;
    velocity.y = 0.0f;

    speed = 100.0f;

    canJump = true;
    canMove = true;

    if (!collision.isColliding(character, bgSprite) && canMove && !onCollideWindow(window))
    {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        velocity.x -= speed;
        isMoving = true;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        velocity.x += speed;
        isMoving = true;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        velocity.y -= speed;
        isMoving = true;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        velocity.y += speed;
        isMoving = true;
    }
    // else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && canJump)
    // {
    //     canJump = false;
    //     velocity.y = -sqrtf(2.0f * 981.0f * jumpHeight);
    //     std::cout << "Jumping.." << "\n";
    // }
    else
    {
        isMoving = false;
    }
    }
    // else
    // {
    //     canMove = false;
    //     velocity.x = 0.0f;
    //     velocity.y = 0.0f;
    // }

    // velocity.y += 981.0f * deltaTime;
}

void Player::updateAnimations(float deltaTime)
{
    if (isMoving)
    {
        if (velocity.x == 0.0f && velocity.y == 0.0f)
        {
            row = 0;
        }
        else if (velocity.x < 0.0f)
        {
            row = 1;
        }
        else if (velocity.x > 0.0f)
        {
            row = 2;
        }
        else if (velocity.y < 0.0f)
        {
            row = 3;
        }
        else if (velocity.y > 0.0f)
        {
            row = 0;
        }

        animation->Update(row, deltaTime);
        this->character.setTextureRect(animation->uvRect);
        this->character.move(velocity * deltaTime);
        // std::cout << velocity.x << " " << velocity.y << "\n";
        clock.restart();
    }
    else
    {
        this->character.setTextureRect(this->currentFrame);
    }
}

// void Player::Update(float deltaTime)
// {
//     velocity.x = 0.9f;
//     velocity.y = 0.0f;

//     speed = 100.0f;

//     canJump = true;

//     if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
//     {
//         velocity.x -= speed;
//         isMoving = true;
//     }
//     else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
//     {
//         velocity.x += speed;
//         isMoving = true;
//     }
//     else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
//     {
//         velocity.y -= speed;
//         isMoving = true;
//     }
//     else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
//     {
//         velocity.y += speed;
//         isMoving = true;
//     }
//     // else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && canJump)
//     // {
//     //     canJump = false;
//     //     velocity.y = -sqrtf(2.0f * 981.0f * jumpHeight);
//     //     std::cout << "Jumping.." << "\n";
//     // }
//     else
//     {
//         isMoving = false;
//     }

//     // velocity.y += 981.0f * deltaTime;

//     if (isMoving)
//     {
//         if (velocity.x == 0.0f && velocity.y == 0.0f)
//         {
//             row = 0;
//         }
//         else if (velocity.x < 0.0f)
//         {
//             row = 1;
//         }
//         else if (velocity.x > 0.0f)
//         {
//             row = 2;
//         }
//         else if (velocity.y < 0.0f)
//         {
//             row = 3;
//         }
//         else if (velocity.y > 0.0f)
//         {
//             row = 0;
//         }

//         animation->Update(row, deltaTime);
//         this->setTextureRect(animation->uvRect);
//         this->move(velocity * deltaTime);
//         // std::cout << velocity.x << " " << velocity.y << "\n";
//         clock.restart();
//     }
//     else
//     {
//         this->setTextureRect(this->currentFrame);
//     }
// }

void Player::render(sf::RenderTarget& target)
{
    target.draw(bgSprite);
    target.draw(this->character);
}
