#pragma once

#include <SFML/Graphics/Sprite.hpp>

class Collision
{
    public:
        Collision();
        ~Collision();

    private:

    public:
        bool isColliding(sf::Sprite& sprite1, sf::Sprite& sprite2);
};
