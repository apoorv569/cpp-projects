#include "Collision.hpp"

Collision::Collision()
{

}

Collision::~Collision()
{

}

bool Collision::isColliding(sf::Sprite &sprite1, sf::Sprite &sprite2)
{
    if (sprite1.getGlobalBounds().intersects(sprite2.getGlobalBounds()))
    {
        return true;
    }

    return false;
}
