#! /bin/bash

# cd where the script is
cd "$(dirname "$0")"

 rm -r build
CXX=g++ meson build
cd build
meson compile -j 4

# run the program
./Platformer
